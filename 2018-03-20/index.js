// Get the express module to ping the bot every 5 minutes. Look at the idiots guide. This is also being supported by Uptime Robot.
const http = require('http');
const express = require('express');
const app = express();
app.get("/", (request, response) => {
  console.log(Date.now() + " Ping Received");
  response.sendStatus(200);
});
app.listen(process.env.PORT);
setInterval(() => {
  http.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 280000);

//THIS IS WHERE YOU CAN ADD CODE FROM VISUAL STUDIO. THE ABOVE LINES PING THE BOT EVERY 5 MINUTES
//VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
//Pick up the libraries/commands from the top here
const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./config.json");
const fs = require("fs");
const clean = text => {
  if (typeof(text) === "string")
    return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
  else
      return text;
};

//Print text in the CMD when the bot is finished starting up
client.on("ready", () => {
  console.log("The bot finished loading. Check your discord!");
  console.log(`Started at ${new Date()}`);
});

//Set game to be playing.
client.on('ready', () => {
  client.user.setActivity("with 'n!help' and a samosa plushy")
});

client.on("message", (message) => {
  
  //Arguments
  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  
  ///Prefix checking, canceling if prefix is not valid, and botception prevention
  if (!message.content.startsWith(config.prefix) || message.author.bot) return;
  
  //message commands are used for a bit
  
  if (command === "help") {
    const embed = new Discord.RichEmbed()
  .setTitle("Help Utility for Nick India Bot")
  .setAuthor("Motu And Patlu", "https://cdn.discordapp.com/emojis/[REMOVED].png?v=1")
  .setColor(0xFF8000)
  .setDescription("Here is a list of all the current command of Nick India Bot!")
  .setFooter("More commands will be added in the future!", "https://cdn.discordapp.com/emojis/[REMOVED].png?v=1")
  .setThumbnail("http://www.animationmagazine.net/wordpress/wp-content/uploads/nick-logo-1504.jpg")
  .setTimestamp()
  .addField("Jeez, you actually want to use this bot that much that you type in n!help? Well, I'm suprised honestly...",
    "Also, for your information, the current prefix is " + config.prefix + " and can not be changed by anyone but Time and Space.")
  .addBlankField(true)
  .addField("ping:",
    "P O N G ! View the ms of the bot.")
  .addField("say [text]",
    "Get the bot to say anything, either in plain text or in an embed (with n!embed).")
  .addField("asl [arg1] [arg2] [arg3]",
    "Really just a demonstration of arguments. Type the command and then your ASL, and the bot will reply with a customised message.")
  .addField("myavatar",
    "Get a downloadable picture of your avatar!")
  .addField("dab",
    "D SE DAB")
  .addField("listemojis",
    "List all the amazing emojis in the server.")
  .addField("noswearing",
    "I'M SORRY THIS IS A CHRISTAIN SERVER!")
  .addField("translate",
    "A google translate utility. Do " + config.prefix + "translate for help.")

  message.author.send({embed});
  message.channel.send("The list is quite long, so I sent it to your direct messages. :thumbsup:")
  }
  
  if (command === "ping") {
    let time = new Date().getTime() - message.createdTimestamp
    message.channel.send(":date: + :stopwatch: - :envelope_with_arrow: =    " + time + "ms\n" + "\n" + ":heartbeat: = " + Math.floor(client.ping) + "ms\n" + "\n" + "***PONG!*** :ping_pong:")
  }

  if (command === "prefix") {
    //Generic message
    message.channel.send("FATAL ERROR: Prefix couldn't be changed, error at line 93. Restarting Nick India core services...")
    //Verify as owner.
    if(message.author.id !== config.ownerID) return;
    else;
    // Gets the prefix from the command (eg. "!prefix +" it will take the "+" from it)
    let newPrefix = message.content.split(" ").slice(1, 2)[0];
    // change the configuration in memory
    config.prefix = newPrefix;
  
    // Now we have to save the file.
    fs.writeFile("./config.json", JSON.stringify(config), (err) => console.error);
    //Send a message to confirm
    message.channel.send("Never mind, I just realised that you're my owner and that I have to work properly for you, or else I'll get rewritten again. The prefix was changed to " + config.prefix)
  }

  if(command === "say") {
    message.delete(200)
    let [word1plain] = args;
    let text = args.slice(1).join(" ");
    message.channel.send(`${word1plain} ` + text);
  }

  if(command === "embed") {
    message.delete(200)
    let [word1] = args;
    let text = args.slice(1).join(" ");
    message.channel.send({embed: {
      color: 0xFF8000,
      description: `${word1} ` + text
    }})
  }

  if (command === "myavatar") {
    message.channel.send(message.author.avatarURL);
    message.channel.send("There's your avatar!")
  }
  
  if (command === "dab") {
    const embed = new Discord.RichEmbed()
  .setTitle("Ａ Ａ Ｊ   Ｋ Ａ Ｈ   Ｔ Ｏ Ｐ Ｉ Ｃ   Ｈ Ａ Ｉ   Ｄ   Ｓ Ｅ   Ｄ Ａ Ｂ")
  .setAuthor("Chotu", "https://cdn.discordapp.com/emojis/[REMOVED].png?v=1")
  /*
   * Alternatively, use "#00AE86", [0, 174, 134] or an integer number.
   */
  .setColor(0xFF8000)
  .setDescription("Motu Patlu? Present! Gattu Battu? Present! Ninja? Present! Kenichi? PRESENT!")
  .setFooter("Nick India™", "https://cdn.discordapp.com/emojis/[REMOVED].png?v=1")
  .setImage("https://cdn.glitch.com/[REMOVED]%2Funnamed.gif?[REMOVED]")
  .setThumbnail("http://www.animationmagazine.net/wordpress/wp-content/uploads/nick-logo-1504.jpg")
  /*
   * Takes a Date object, defaults to current date.
   */
  .setTimestamp()
  .setURL("http://www.nickindia.com/")
  
  //.addBlankField(true)
  
  message.channel.send({embed});
  }

  if (command === "eval") {
    if(message.author.id !== config.ownerID) return;
    try {
      const code = args.join(" ");
      let evaled = eval(code);

      if (typeof evaled !== "string")
        evaled = require("util").inspect(evaled);

      message.channel.send(clean(evaled), {code:"xl"});
    } catch (err) {
      message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
    }
  }

  if (command === "listemojis") {
    const emojiList = message.guild.emojis.map(e=>e.toString()).join(" ");
    message.channel.send(emojiList);
  }
  
  if(command === "translate"){
    let [exitlang] = args;
    let text = args.slice(1).join(" ");
    const translate = require('google-translate-api');

    translate(`${text}`, {to: `${exitlang}`}).then(res => {
    console.log(res);
    console.log(res.text);
    //=> Ik spea Nederlands!
    console.log(res.from.text.autoCorrected);
    //=> false
    console.log(res.from.text.value);
    //=> I [speak] Dutch!
    console.log(res.from.text.didYouMean);
    //=> true
    message.channel.send(res.text);
  }).catch(err => {
    console.error(err);
    message.channel.send(":x: Error!\n\nUse:\n" + config.prefix + "translate [translate-to] [text]\n\nThe [translate-from] language is auto-detected. The [translate-to] language requires a code. \nFind it at: https://cloud.google.com/translate/docs/languages")
  });
  }
  
  if (command === "randomnumber") {
    let [number1, number2] = args;
    message.channel.send(Math.floor((Math.random() * `${number2}`) + `${number1}`));
  }
  
  if (command === "streammp3") {
    // Only try to join the sender's voice channel if they are in one themselves
    if (message.member.voiceChannel) {
      let url = args[0]
      message.member.voiceChannel.join()
        .then(connection => { // Connection is an instance of VoiceConnection
          const dispatcher = connection.playStream(`${url}`);
          message.channel.send("exec autoexec.cfg");
        })
        .catch(console.log);
    } else {
      message.reply("FATAL ERROR AT LINE 208: You didn't join a voice channel.");
    }
  }
 
  if (command === "restart") {
    if(message.author.id !== config.ownerID) message.channel.send("Get out.");
    if(message.author.id !== config.ownerID) return;
    else
    process.exit();
  }
  
  if (command === "playjkhskjfskj") {
    if (!args[1]) {
      message.channel.send("Please provide a link!")
    }
  }
  
  //ytdl-core placeholder
  
});

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//CODE LINE ENDS HERE.
//THIS TOKEN IS STORED IN THE .env FILE. IT IS NOT COMPATIBLE WITH THE VISUAL STUDIO CODE VERSION!
client.login(process.env.TOKEN);

//Resume from https://anidiotsguide_old.gitbooks.io/discord-js-bot-guide/content/getting-started/the-long-version.html
