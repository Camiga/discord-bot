// Make errors look better (remove this to debug).
process.on('unhandledRejection', error => console.error(`Unhandled Promise Rejection:\n${error}`));

const moment = require('moment-timezone');
moment.tz.setDefault('Australia/Sydney');

// Create website using native http module.
const http = require('http');
http.createServer(function(req, res) {
	res.end();
	console.log(`[${moment().format('DD-MM-YYYY hh:mm:ss zz')}] - Pinged`);
}).listen(process.env.PORT);

// Ping every ~2.5 minutes.
const https = require('https');
setInterval(() => {
	https.get(`https://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 150000);

// Requirements
const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require('fs');
const ownerID = process.env.OWNERID;
const Canvas = require('canvas');
const fetch = require('node-fetch');
const SQLite = require('better-sqlite3');
const sql = new SQLite('./scores.sqlite');

client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./cmds');
for (const file of commandFiles) {
	const command = require(`./cmds/${file}`);

	// set a new item in the Collection
	// with the key as the command name and the value as the exported module
	client.commands.set(command.name, command);
}

const cooldowns = new Discord.Collection();

// on ready event for setting sqlite table. read anidiots.guide for more info
client.on('ready', () => {
	// Check if the table "points" exists.
	const table = sql.prepare('SELECT count(*) FROM sqlite_master WHERE type=\'table\' AND name = \'scores\';').get();
	if (!table['count(*)']) {
		// If the table isn't there, create it and setup the database correctly.
		sql.prepare('CREATE TABLE scores (id TEXT PRIMARY KEY, user TEXT, guild TEXT, points INTEGER, level INTEGER);').run();
		// Ensure that the "id" row is always unique and indexed.
		sql.prepare('CREATE UNIQUE INDEX idx_scores_id ON scores (id);').run();
		sql.pragma('synchronous = 1');
		sql.pragma('journal_mode = wal');
	}

	// And then we have two prepared statements to get and set the score data.
	client.getScore = sql.prepare('SELECT * FROM scores WHERE user = ? AND guild = ?');
	client.setScore = sql.prepare('INSERT OR REPLACE INTO scores (id, user, guild, points, level) VALUES (@id, @user, @guild, @points, @level);');

	// set playing status and print in console
	client.user.setActivity('with leftover samosas | n!help');
	console.log(`[${moment().format('DD-MM-YYYY hh:mm:ss zz')}] - Startup Complete`);
});

// message in case of disconnect
client.on('disconnect', () =>{
	console.log(`[${moment().format('DD-MM-YYYY hh:mm:ss zz')}] - Disconnected`);
});

// message in case of reconnecting
client.on('reconnecting', () => {
	console.log(`[${moment().format('DD-MM-YYYY hh:mm:ss zz')}] - Reconnecting`);
	process.exit();
});

// when a member is added to the guild
client.on('guildMemberAdd', async member => {
	// Check for the channel first
	const channel = member.guild.channels.find(ch => ch.name === 'taxi-dropoffs');
	if (!channel) return;
	// Pass the entire Canvas object because you'll need to access its width, as well its context
	const applyText = (canvas, text) => {
		const ctx = canvas.getContext('2d');

		// Declare a base size of the font
		let fontSize = 70;

		do {
			// Assign the font to the context and decrement it so it can be measured again
			ctx.font = `${fontSize -= 10}px URW Gothic`;
			// Compare pixel width of the text to the canvas minus the approximate avatar size
		} while (ctx.measureText(text).width > canvas.width - 300);

		// Return the result to use in the actual canvas
		return ctx.font;
	};

	const canvas = Canvas.createCanvas(640, 360);
	const ctx = canvas.getContext('2d');

	// Create a buffer to load the background image with
	const prepareBgImg = await fetch('https://cdn.glitch.com/[REMOVED]%2FdabFINAL.png?[REMOVED]');
	const finalBgImg = await prepareBgImg.buffer();
	const background = await Canvas.loadImage(finalBgImg);
	ctx.drawImage(background, 0, 0, canvas.width, canvas.height);

	ctx.strokeStyle = '#74037b';
	ctx.strokeRect(0, 0, canvas.width, canvas.height);

	// Slightly smaller text placed above the member's display name
	ctx.font = '30px URW Gothic';
	ctx.fillStyle = '#ffffff';
	ctx.fillText('Oofs, please welcome:', canvas.width / 2.4, canvas.height / 4);

	// Add an exclamation point here and below
	ctx.font = applyText(canvas, `${member.user.username}`);
	ctx.fillStyle = '#fffb00';
	// ctx.fillText(`${member.displayName}`, canvas.width / 2.3, canvas.height / 2.5);
	ctx.fillText(`${member.user.username}`, canvas.width / 2.3, canvas.height / 2.5);

	// small text under username
	ctx.font = '18px URW Gothic';
	ctx.fillStyle = '#ffffff';
	ctx.fillText(`We now have ${member.guild.members.size} members!`, canvas.width / 2.0, canvas.height / 1.3);

	ctx.beginPath();
	ctx.arc(425, 210, 50, 0, Math.PI * 2, true);
	ctx.closePath();
	ctx.clip();
	// ctx.stroke()

	const prepareAvImg = await fetch(member.user.displayAvatarURL);
	const finalAvImg = await prepareAvImg.buffer();
	const avatar = await Canvas.loadImage(finalAvImg);
	ctx.drawImage(avatar, 375, 160, 100, 100);

	const attachment = new Discord.Attachment(canvas.toBuffer(), 'nick-welcome-image.png');

	channel.send(`Welcome to the server, ${member}!`, attachment);
});

// when a member is removed from the guild
client.on('guildMemberRemove', async member => {
	const channel = member.guild.channels.find(ch => ch.name === 'taxi-dropoffs');
	if (!channel) return;

	channel.send(`**${member.user.username}** has left the server. Have fun, and thanks for flying with us!`);
});

// on message (async added to support await)
client.on('message', async message => {

	// Don't respond to bots or direct messages.
	if (message.author.bot) return;
	if (!message.guild) return;

	// Point system must only work in guilds.
	let score;
	if (message.guild) {
		// This is where we'll put our code.
		score = client.getScore.get(message.author.id, message.guild.id);
		if (!score) {
			score = { id: `${message.guild.id}-${message.author.id}`, user: message.author.id, guild: message.guild.id, points: 0, level: 1 };
		}

		score.points++;
		const curLevel = Math.floor(0.2 * Math.sqrt(score.points));
		if(score.level < curLevel) {
			message.reply(`You leveled up to level **${curLevel}**! Yay fake internet points!`);
			score.level++;
		}
		client.setScore.run(score);
	}

	// Allow changing between multiple prefixes.
	// set the default prefix of 'n!' but allow it to change if a message starts with something else supported.
	let prefix = process.env.PREFIX_A;
	if (message.content.startsWith(process.env.PREFIX_A.toUpperCase())) {
		prefix = process.env.PREFIX_A.toUpperCase();
	}
	else if (message.content.startsWith(process.env.PREFIX)) {
		prefix = process.env.PREFIX_B;
	}
	else if (message.content.startsWith(process.env.PREFIX_B.toUpperCase())) {
		prefix = process.env.PREFIX_B.toUpperCase();
	}
	else if (message.content.startsWith(`<@${client.user.id}>`)) {
		prefix = `<@${client.user.id}>`;
	}

	// Arguments (needs to be put before anonymous message sending).
	const args = message.content.slice(prefix.length).trim().split(/ +/g);
	const commandName = args.shift().toLowerCase();

	// Set up anonymous message sending (must be put before prefix checking again).
	if(message.content.startsWith('>>>') || message.content.split(' ')[message.content.split(' ').length - 1] == '>>>') {
		await message.delete(300);
		const anonWeb = await message.channel.createWebhook('...', 'https://cdn.glitch.com/[REMOVED]%2FDiscord-Logo-Color?[REMOVED]');

		if(message.content.startsWith('>>>')) {
			const anonEmbed = new Discord.RichEmbed()
				.setDescription(args.join(' '))
				.setColor('7289DA');
			await anonWeb.send(anonEmbed);
		}
		else {
			const anonEmbed = new Discord.RichEmbed()
				.setDescription(message.content.substring(0, message.content.length - 3))
				.setColor('7289DA');
			await anonWeb.send(anonEmbed);
		}

		await anonWeb.delete();
	}

	// prefix checking (must be put after point system and anonymous message sending)
	if (!message.content.startsWith(prefix)) return;

	const command = client.commands.get(commandName)
      || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

	// if (!command) return;
	if (!command) return message.reply(`that's not a command! Do \`${process.env.PREFIX_A}help\` to see our commands.`);
	// simplify guildOnly commands
	if (command.guildOnly && message.channel.type !== 'text') return message.reply('I can\'t execute that command inside DMs!');
	// simplify ownerOnly commands
	if (command.ownerOnly && message.author.id !== ownerID) return message.reply('that command is off limits to you. Sorry.');
	// delete messages once used (in guilds)
	if (command.deleteOnUse && message.channel.type == 'text') await message.delete(300);

	// if arguments are needed
	if (command.args && !args.length) {
		let reply = `You didn't provide any arguments, ${message.author}!`;
		if (command.usage) {
			reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
		}
		return message.channel.send(reply);
	}

	if (!cooldowns.has(command.name)) {
		cooldowns.set(command.name, new Discord.Collection());
	}

	const now = Date.now();
	const timestamps = cooldowns.get(command.name);
	const cooldownAmount = (command.cooldown || 0) * 1000;

	if (!timestamps.has(message.author.id)) {
		if (!timestamps.has(message.author.id)) {
			timestamps.set(message.author.id, now);
			setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
		}
	}
	else {
		const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

		if (now < expirationTime) {
			const timeLeft = (expirationTime - now) / 1000;
			return message.reply(`Oof wait ${timeLeft.toFixed(1)} second(s) before you can use the \`${command.name}\` command.`);
		}

		timestamps.set(message.author.id, now);
		setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
	}

	try {
		command.execute(client, message, args, Discord);
	}
	catch (e) {
		console.error(e);
		message.reply('Something went wrong over here.');
	}
});

// Token is stored in the .env
client.login(process.env.TOKEN);
console.log('made it here');
