module.exports = {
	name: 'fj',
	description: 'Does some even more extra secret stuff.',
	usage: false,
	options: false,
	aliases: false,
	cooldown: 0,
	args: false,
	guildOnly: true,
	ownerOnly: true,
	async execute(client, message) {
		client.emit('guildMemberAdd', message.member || await message.guild.fetchMember(message.author));
		message.channel.send('Done.');
	},
};