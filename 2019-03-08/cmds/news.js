module.exports = {
	name: 'news',
	description: false,
	usage: false,
	options: false,
	aliases: false,
	cooldown: 0,
	args: false,
	guildOnly: true,
	ownerOnly: true,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		if(args[0] == '-s') {
			// "P" webhook
			const Hook = new Discord.WebhookClient(process.env.NEWS_ID_P, process.env.NEWS_TOKEN_P);
			Hook.send(process.env.PUBLIC_ROLE + ' INCOMING NEWS! ' + args.slice(1).join(' '));
		}
		else {
			// Regular webhook
			const Hook = new Discord.WebhookClient(process.env.NEWS_ID, process.env.NEWS_TOKEN);
			Hook.send(process.env.PUBLIC_ROLE + ' INCOMING NEWS! ' + args.join(' '));
		}
	},
};