module.exports = {
	name: 'dab',
	description: 'DSEDAB',
	usage: false,
	options: false,
	aliases: false,
	cooldown: 2,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args, Discord) {
		const dabEmbed = new Discord.RichEmbed()
			.setColor('FF8000')
			.setTitle('ＡＡＪ　ＨＡ　ＴＯＰＩＣ　ＨＡＩ　Ｄ　ＳＥ　ＤＡＢ！')
			.setURL('http://www.nickindia.com/')
			.setAuthor('Chotu', 'https://cdn.glitch.com/[REMOVED]%2Fchotuface.png?[REMOVED]', 'http://www.nickindia.com/')
			.setDescription('Motu Patlu? Present! Gattu Battu? Present! Ninja? Present! Kenichi? PRESENT!')
			.setThumbnail('https://cdn.glitch.com/[REMOVED]%2Fnicklogo.jpg?[REMOVED]')
			.setImage('https://cdn.glitch.com/[REMOVED]%2Fdab.gif?[REMOVED]')
			.setTimestamp()
			.setFooter('Nick India', 'https://cdn.glitch.com/[REMOVED]%2Fchotupoint.png?[REMOVED]');

		message.channel.send(dabEmbed);
	},
};
