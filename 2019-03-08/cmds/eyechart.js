module.exports = {
	name: 'eyechart',
	description: 'Make a custom eyechart! Just input your text and wait.',
	usage: '[text]',
	options: false,
	aliases: false,
	cooldown: 5,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const notify = await message.channel.send('Processing...please wait.');

		const eyeChart = await new Discord.RichEmbed()
			.setColor('FF8000')
			.setTitle('Your eyechart:')
			.setURL('http://www.eyechartmaker.com/demo.png')
			.setImage(`https://images.weserv.nl/?url=www.eyechartmaker.com/generate.php?line1=${args.join('')}&crop=300,380,40,0`);

		notify.edit(eyeChart);
	},
};