module.exports = {
	name: 'play',
	description: 'Play a video from YouTube using a URL you must provide.',
	usage: '[url]',
	options: false,
	aliases: ['youtube', 'stream'],
	cooldown: 5,
	args: true,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args) {
		const notify = await message.channel.send('Preparing...please wait.');
		const { voiceChannel } = message.member;
		const ytdl = require('ytdl-core');
		const url = args;

		// detect if member is in a voice channel
		if (!voiceChannel) {
			return message.reply('please join a voice channel first!');
		}

		// check if link is a valid YouTube URL
		const linkCheck = ['https://www.youtube.com/watch?v=', 'https://youtu.be/', 'https://m.youtube.com/watch?v='];
		if(!linkCheck.some(word => message.content.includes(word))) {
			message.reply('that\'s not a playable YouTube link.');
			return;
		}

		ytdl.getInfo(`${url}`, function(err, info) {
			// start streaming
			voiceChannel.leave();
			voiceChannel.join().then(connection => {
				notify.edit(`Now playing: **${info.title}**`);

				const stream = ytdl(`${url}`, { filter: 'audioonly' });
				const dispatcher = connection.playStream(stream);

				// when finished
				dispatcher.on('end', () => {
					voiceChannel.leave();
					// edit message to show the song has finished
					message.channel.send(`**${info.title}** has finished playing.`);
				});
			});
			// end for ytdl info
		});
	},
};