module.exports = {
	name: 'fox',
	description: 'Get a random fox to appear in your conversation.',
	usage: false,
	options: false,
	aliases: ['randomfox'],
	cooldown: 3,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const fetch = require('node-fetch');
		const Notify = await message.reply('retrieving your fox...please wait.');
		const Fox = await fetch('https://randomfox.ca/floof/').then(res => res.json());

		const foxEmbed = new Discord.RichEmbed()
			.setColor('FF8000')
			.setTitle('Your fox(es):')
			.setImage(Fox.image);

		await Notify.edit(foxEmbed);
	},
};