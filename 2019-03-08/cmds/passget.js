module.exports = {
	name: 'getnsfw',
	description: 'Get a pass to a secret channel.',
	usage: false,
	options: false,
	aliases: false,
	cooldown: 15,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message) {
		const nsfw = message.guild.roles.find('name', 'NSFW Pass');
		const member = message.member;
		const prefix = process.env.PREFIX_A;
		const Discord = require('discord.js');

		// check to make sure that person doesn't already have the role
		if (message.member.roles.find('name', 'NSFW Pass')) {
			message.channel.send('You already have the role!');
			return;
		}

		const nsfwMessage = new Discord.RichEmbed()
			.setColor('0xFF8000')
			.setTitle('NSFW Pass confirmation')
			.setDescription('***WARNING! The NSFW channel is not for everyone. Only accept if you really want to use it. DON\'T ACCEPT JUST OUT OF CURIOSITY!***')
			.addField('To confirm:', 'React with ✅. The bot has already added it for you.', true)
			.addField('To decline:', 'Leave this message alone. The prompt will time out in about 30 seconds.', true);

		const notify = await message.channel.send(nsfwMessage);

		const filter = (reaction, user) => {
			return reaction.emoji.name === '✅' && user.id === message.author.id;
		};

		const collector = await notify.createReactionCollector(filter, { time: 30000 });

		collector.on('collect', () => {
			member.addRole(nsfw).catch(console.error);
			message.reply('you have been given the role. Have fun?\n***Note: You can always remove the role with \'' + prefix + 'removensfw***\'');
			return;
		});

		collector.on('end', () => {
			notify.delete();
		});

		notify.react('✅');
	},
};