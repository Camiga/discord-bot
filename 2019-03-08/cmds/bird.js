module.exports = {
	name: 'bird',
	description: 'Get a random birb to appear in your conversation.',
	usage: false,
	options: false,
	aliases: ['birb', 'randombird', 'randombirb'],
	cooldown: 3,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const fetch = require('node-fetch');
		const Notify = await message.reply('retrieving your birb, please wait...');
		const Birb = await fetch('http://random.birb.pw/tweet.json').then(res => res.json());

		const birbEmbed = new Discord.RichEmbed()
			.setColor('FF8000')
			.setTitle('Your birb(s):')
			.setImage(`http://random.birb.pw/img/${Birb.file}`);

		await Notify.edit(birbEmbed);
	},
};