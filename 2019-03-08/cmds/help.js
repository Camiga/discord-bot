module.exports = {
	name: 'help',
	description: 'List all of my commands or info about a specific command.',
	usage: '[OPTIONS]',
	options: '[command-name] = Get help on a specific command.',
	aliases: ['commands'],
	cooldown: 3,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args) {
		const { PREFIX_A, OWNERID, ANT_ID, DIP_ID } = process.env;
		const { commands } = message.client;
		const data = [];

		if (!args.length) {
			data.push('Thanks for requesting help! Here are our current commands:\n```');
			data.push(commands.map(command => command.name).join(', '));
			data.push(`\`\`\`\nIf you need help for a specific command, just do \`${PREFIX_A}help [command_name]\`.`);
			message.channel.send(data, { split: true });
			return;
		}
		else {
			if (!commands.has(args[0])) {
				return message.reply('that\'s not a valid command.');
			}

			const command = commands.get(args[0]);
			const Auth = message.author.id;
			let Allowed = false;
			if (Auth == OWNERID || Auth == DIP_ID || Auth == ANT_ID) Allowed = true;

			if (command.ownerOnly && Allowed == false) return message.reply('that command is off-limits. Sorry.');

			data.push(`__**Name:**__ ${command.name}`);

			if (command.description) data.push(`__**Description:**__ ${command.description}`);
			// if there's a usage
			if (command.usage) data.push(`\n__**Usage:**__\`\`\`${PREFIX_A}${command.name} ${command.usage}\`\`\``);
			// if there's no usage at all
			else if (!command.usage) data.push(`\n__**Usage:**__\`\`\`${PREFIX_A}${command.name}\`\`\``);
			if (command.options) data.push(`__**Options:**__\`\`\`${command.options.join('\n\n')}\`\`\``);
			if (command.aliases) data.push(`__**Aliases:**__ ${command.aliases.join(', ')}`);
			if (command.cooldown) data.push(`__**Cooldown:**__ ${command.cooldown} seconds`);
		}
		message.channel.send(data, { split: true });
	},
};