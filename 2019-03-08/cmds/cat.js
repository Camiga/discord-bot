module.exports = {
	name: 'cat',
	description: 'Get a random cat instantly in the middle of your conversation!',
	usage: false,
	options: false,
	aliases: ['randomcat', 'kitty', 'kitten'],
	cooldown: 3,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const fetch = require('node-fetch');
		const Notify = await message.reply('retrieving your cat, please wait...');

		async function sendCat() {
			try {
				const Cat = await fetch('http://aws.random.cat/meow').then(res => res.json());

				const catEmbed = new Discord.RichEmbed()
					.setColor('FF8000')
					.setTitle('Your cat(s):')
					.setImage(Cat.file);

				await Notify.edit(catEmbed);
			}
			catch(error) {
				sendCat();
				console.log(error);
				return;
			}
		}
		sendCat();
	},
};