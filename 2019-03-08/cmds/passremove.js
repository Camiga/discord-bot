module.exports = {
	name: 'removensfw',
	description: 'Remove your pass to the secret channel.',
	usage: false,
	options: false,
	aliases: ['nsfwremove'],
	cooldown: 5,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		const nsfw = message.guild.roles.find('name', 'NSFW Pass');
		const member = message.member;
		// makes sure they actually have the role to remove it.
		if(!message.member.roles.find('name', 'NSFW Pass')) {
			message.reply('you can\'t remove a role you don\'t have, dummy.');
			return;
		}
		// actually gives them the role once verified that they don't have it.
		else {member.removeRole(nsfw).catch(console.error);}
		message.reply('your role has been removed. Thanks for your company?');
	},
};