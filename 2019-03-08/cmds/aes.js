module.exports = {
	name: 'aesthetics',
	description: 'Write anything in ＡＥＳＴＨＥＴＩＣＳ',
	usage: '[text]',
	options: false,
	aliases: ['aesthetic', 'aes'],
	cooldown: 2,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: true,
	execute(client, message, args) {
		message.channel.send(args.slice(0).join(' ').replace(/[a-zA-Z0-9!?.'";:\][}{)(@#$%^&*\-_=+`~><]/g, (c) => String.fromCharCode(0xFEE0 + c.charCodeAt(0))).replace(/ /g, '　'));
	},
};