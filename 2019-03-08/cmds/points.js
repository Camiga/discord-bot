module.exports = {
	name: 'points',
	description: 'View how many points you have, as well as what level you are.',
	usage: false,
	options: false,
	aliases: ['score'],
	cooldown: 5,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		const score = client.getScore.get(message.author.id, message.guild.id);
		return message.reply(`You currently have ${score.points} point(s) and are level ${score.level}!`);
	},
};