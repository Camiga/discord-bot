module.exports = {
	name: 'purge',
	description: 'A command to delete messages in bulk.',
	usage: '[OPTIONS] [ammount]',
	options: '[@mention] = Get `[ammount]` of messages and only delete them from the user mentioned.',
	aliases: ['delete', 'prune'],
	cooldown: 0,
	args: true,
	guildOnly: true,
	ownerOnly: true,
	deleteOnUse: true,
	execute(client, message) {
		const user = message.mentions.users.first();
		const amount = parseInt(message.content.split(' ')[1]) ? parseInt(message.content.split(' ')[1]) : parseInt(message.content.split(' ')[2]);
		if (!amount) return message.reply('Must specify an amount to delete!');
		if (!amount && !user) return message.reply('Must specify a user and amount, or just an amount, of messages to purge!');
		message.channel.fetchMessages({
			limit: amount,
		}).then((messages) => {
			if (user) {
				const filterBy = user ? user.id : client.user.id;
				messages = messages.filter(m => m.author.id === filterBy).array().slice(0, amount);
			}
			message.channel.bulkDelete(messages).catch(error => console.log(error.stack));
		});
	},
};