module.exports = {
	name: 'flip',
	description: 'Flip your text upside down!',
	usage: '[text]',
	options: false,
	aliases: ['fliptext'],
	cooldown: 3,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args) {
		const mapping = '¡"#$%⅋,)(*+\'-˙/0ƖᄅƐㄣϛ9ㄥ86:;<=>¿@∀qƆpƎℲפHIſʞ˥WNOԀQɹS┴∩ΛMX⅄Z[/]^_`ɐqɔpǝɟƃɥᴉɾʞlɯuodbɹsʇnʌʍxʎz{|}~';
		// Start with the character '!'
		const OFFSET = '!'.charCodeAt(0);
		message.channel.send(
			args.join(' ').split('')
				.map(c => c.charCodeAt(0) - OFFSET)
				.map(c => mapping[c] || ' ')
				.reverse().join('')
		);
	},
};