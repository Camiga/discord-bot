module.exports = {
	name: 'leave',
	description: 'Make bot leave a voice channel if you\'re also connected to said channel.',
	usage: false,
	options: false,
	aliases: ['disconnect', 'stop'],
	cooldown: 10,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		const { voiceChannel } = message.member;
		if(!voiceChannel) {
			message.reply('you aren\'t in a voice channel.');
			return;
		}
		voiceChannel.leave();
		message.react('✅');
	},
};