module.exports = {
	name: 'number',
	description: 'Generate a random number.',
	usage: '[minimum-number] [maximum-number]',
	options: false,
	aliases: ['nm'],
	cooldown: 1,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args) {
		const [number1, number2] = args;

		if (!args[1]) return message.reply('you didn\'t give me at least 2 numbers.');
		if (isNaN(Math.round((Math.random() * number2) + number1))) return message.reply('your answer is Not a Number.');

		message.channel.send(Math.round((Math.random() * number2) + number1));
	},
};