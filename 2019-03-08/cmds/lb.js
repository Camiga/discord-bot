module.exports = {
	name: 'leaderboard',
	description: 'See the top 10 point leaders for the current guild.',
	usage: false,
	options: false,
	aliases: ['lb'],
	cooldown: 5,
	args: false,
	guildOnly: true,
	ownerOnly: true,
	deleteOnUse: false,
	execute(client, message) {
		// definitions
		const SQLite = require('better-sqlite3');
		const sql = new SQLite('./scores.sqlite');
		const Discord = require('discord.js');

		const top10 = sql.prepare('SELECT * FROM scores WHERE guild = ? ORDER BY points DESC LIMIT 10;').all(message.guild.id);

		// Now shake it and show it! (as a nice embed, too!)
		const embed = new Discord.RichEmbed()
			.setTitle('Leaderboard')
			.setAuthor(client.user.username, client.user.avatarURL)
			.setDescription('The top 10 people in this guild with the highest number of fake internet points!')
			.setColor(0xFF8000);

		for(const data of top10) {
			// This might get rid of unknown users and stop the leaderboard from breaking, but I can't test if it works or not.
			if(client.users.get(data.user).tag == undefined) continue;
			embed.addField(client.users.get(data.user).tag, `${data.points} points (level ${data.level})`);
		}
		return message.channel.send({ embed });
	},
};