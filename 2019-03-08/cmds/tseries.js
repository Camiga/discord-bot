module.exports = {
	name: 'tseries',
	description: 'View the battle between PewDiePie and T-Series!',
	usage: false,
	aliases: ['pewdiepie', 'youtube', 't-series', 'subs', 't'],
	cooldown: 15,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const Notify = await message.channel.send('This will only take a moment.\n**25% complete.**');
		const fetch = require('node-fetch');
		const Data = [];

		async function getSubs(Type, Name) {
			const body = await fetch(`https://www.youtube.com/${Type}/${Name}`).then(res => res.text());
			// "subscriber-count" contains subscribers number near it.
			// Find how far in you have to go to get there.
			const lineStart = body.indexOf('subscriber-count');
			// Get ~90 characters infront of lineStart.
			const fullLine = body.substr(lineStart, 90);
			// Use regex to get the number.
			const Subs = fullLine.match(/[0-9,]/g).join('');
			// Push subs to array
			await Data.push(Subs);
		}

		async function getVideo(Video) {
			const body = await fetch(`https://www.youtube.com/watch?v=${Video}`).then(res => res.text());

			// Find strings containing the numbers.
			const Views = await body.substr(body.indexOf('h-v'), 40).match(/[0-9,]/g).join('');
			const Likes = await body.substr(body.indexOf('like this '), 50).match(/[0-9,]/g).join('');
			const Dislikes = await body.substr(body.indexOf('slike this '), 50).match(/[0-9,]/g).join('');

			// Send all data to the VidData array.
			await Data.push(Views, Likes, Dislikes);
		}


		// Do functions to add numbers to array. Also show progress.
		await getSubs('user', 'PewDiePie');
		Notify.edit('This will only take a moment.\n**50% complete.**');
		await getSubs('channel', 'UCq-Fj5jknLsUf-MWSy4_brA');
		Notify.edit('This will only take a moment.\n**75% complete.**');
		await getVideo('YbJOTdZBX1g');

		// Calculate difference in subscribers.
		function calcDiff(Input1, Input2) {
			// Convert strings into numbers.
			const Num1 = Number(Input1.replace(',', '').replace(',', ''));
			const Num2 = Number(Input2.replace(',', '').replace(',', ''));
			// Subtract eachother.
			const Subtract = Num1 - Num2;
			// Return as a string with commas back.
			return Subtract.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
		}

		// Break up embed sentences. (This isn't needed, just looks nicer)
		const Help1 = '[If you haven\'t already, click here to subscribe to PewDiePie.';
		const Help2 = 'It takes less than a minute and has no downside.]';
		const Subsc = '(https://www.youtube.com/user/PewDiePie?sub_confirmation=1)';
		const doIt1 = 'You might think you\'re only one person, but that still makes a difference.';
		const doIt2 = 'It\'s the reason this war has gone for so long.';

		const Results = await new Discord.RichEmbed()
			.setColor('FF8000')
			.setTitle('PewDiePie vs T-Series (And Rewind)')
			.setURL('https://www.youtube.com/user/PewDiePie?sub_confirmation=1')
			.setDescription(`PewDiePie and T-Series are __**${calcDiff(Data[0], Data[1])}**__ subscribers apart.`)
			.addField('PewDiePie', Data[0], true)
			.addField('T-Series', Data[1], true)
			.addField('Rewind 2018', `Views:                  ${Data[2]}\nLikes:                        ${Data[3]}\nDislikes:                 ${Data[4]}\n**Dislikes - Likes:**   __**${calcDiff(Data[4], Data[3])}**__`)
			.addBlankField()
			.addField('We need your help.', `${Help1} ${Help2}${Subsc}`)
			.addField('Just do it.', `${doIt1} ${doIt2}`)
			.addBlankField()
			.setTimestamp()
			.setFooter('v1.1.4 | Always get the latest results. | n!tseries | n!t');

		await Notify.edit(Results);
	},
};