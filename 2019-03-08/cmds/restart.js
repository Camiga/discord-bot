module.exports = {
	name: 'restart',
	description: 'Restart the bot.',
	usage: false,
	options: false,
	aliases: ['refresh', 're', 'r'],
	cooldown: 0,
	args: false,
	guildOnly: false,
	ownerOnly: true,
	deleteOnUse: false,
	async execute(client, message) {
		// defining moment-timezone
		const moment = require('moment-timezone');
		moment.tz.setDefault('Australia/Sydney');

		await message.channel.send('Restarting...');
		await console.log(`Manual shutdown started on: ${moment().format('DD-MM-YYYY hh:mm:ssA zz')}`);
		await process.exit();
	},
};