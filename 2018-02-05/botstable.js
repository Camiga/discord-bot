//Pick up the libraries/commands from the top here
const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./config.json");
const fs = require("fs")
//const args = message.content.slice(prefix.length).trim().split(/ +/g); RE-ENABLE FOR ARGUMENTS ONLY
//const command = args.shift().toLowerCase();

//Print text in the CMD when the bot is finished starting up
client.on("ready", () => {
  console.log("Startup Complete. Delivering Samosas...Just kidding. The bot finished loading. Check your discord!");
});

//Set game to be playing.
client.on('ready', () => {
  client.user.setActivity('Hot Potato with a fresh Samosa')
});

client.on("message", (message) => {
  ///Prefix checking, canceling if prefix is not valid, and botception prevention
  if (!message.content.startsWith(config.prefix) || message.author.bot) return;
  
  //message commands are used for a bit
  
  if (message.content.startsWith(config.prefix + "help")) {
    message.channel.send("Sorry, this is a very basic help utility as I am still trying to figure out how to proprely format all of this. Here are all the commands for the time being: `help`, `ping`, `samosa`, `clap`, `info`, `prefix`, `admin`, and `embed`.")
  }
  
  if (message.content.startsWith(config.prefix + "ping")) {
    message.channel.send(new Date().getTime() - message.createdTimestamp + " ms catch time. PONG!")
  }

  if (message.content.startsWith(config.prefix + "samosa")) {
    message.channel.send("Your samosa is being cooked! Well, Time and Space really hasn't finished this command yet, but here's some stuff I found on google images for you instead!")
    message.channel.send("https://indianhealthyrecipes.com/wp-content/uploads/2016/09/samosa-recipe-snacks-recipes.jpg")
    message.channel.send("https://www.ndtv.com/cooks/images/moong-dal-samosa-new.jpg")
    message.channel.send("Enjoy your overdose in samosas.")
  }

  if (message.content.startsWith(config.prefix + "clap")) {
    message.channel.send("You're so clapped.")
    message.channel.send(":clap: :clap: :clap: :clap: :clap:")
  }
  
  if (message.content.startsWith(config.prefix + "info")) {
    message.channel.send("SamosaBot is currently being developed by Time and Space using discord.js.")
    message.channel.send("Dipsmash was the one that brought up the idea of creating a bot as well as a procedure for it (basically does half of the stuff in the server anyways...)")
    message.channel.send("Sorry, but there really isn't much else to display here...This bot will be updated with more commands later on, so you can use [s!help] to check up on its progress any time!")
  }

  if(message.content.startsWith(config.prefix + "prefix")) {
    // Gets the prefix from the command (eg. "!prefix +" it will take the "+" from it)
    let newPrefix = message.content.split(" ").slice(1, 2)[0];
    // change the configuration in memory
    config.prefix = newPrefix;
  
    // Now we have to save the file.
    fs.writeFile("./config.json", JSON.stringify(config), (err) => console.error);
  }

  //Administrator only command demonstration. Refer to config.json for config.ownerID
  if (message.content.startsWith(config.prefix + "admin")) {
    if(message.author.id !== config.ownerID) return;
    else message.channel.send("You are Time and Space! This command has been validated for you!")
  }

  //Test embedded command.
  if (message.content.startsWith(config.prefix + "embed")) {
    message.channel.send({embed: {
      color: 0x00FF2A,
      description: "If you wanted to change the colour of the embed to lets say 00FF2A (which is the current colour displayed on the side), you need to add 0x to the front, so you add 0x00FF2A to the colour tag."
    }})
  }

});

client.login(config.token);

//Resume from https://anidiotsguide_old.gitbooks.io/discord-js-bot-guide/content/getting-started/the-long-version.html