const { prefix } = require("../config.json")
module.exports = {
    name: 'embed',
    description: 'Make the bot say something, just with a nice looking embed.',
    usage: '[text]',
    aliases: ['sayembed'],
    cooldown: 1,
    execute(client, message, args) {
      let text = args.slice(0).join(" ");
      if (!args[0]) {
        message.reply("you didn't give me anything to embed.")
        return;
      }
      
      message.delete(200);
      message.channel.send({embed: {
      color: 0xFF8000,
      description: text
    }})
  }
}