module.exports = {
    name: 'randomnumber',
    description: 'Generate a random number using pure JavaScript!',
    usage: '[minimum] [maximum]',
    aliases: ['number'],
    cooldown: 1,
    execute(client, message, args) {
      let [number1, number2] = args;
      if (!args[1]) {
      message.reply("you didn't give me at least 2 numbers")
      return;
      }
      
      message.channel.send("Your random number is: " + Math.round((Math.random() * `${number2}`) + `${number1}`));
    }
}