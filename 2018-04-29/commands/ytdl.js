module.exports = {
    name: 'play',
    description: 'Play a video URL from YouTube. Only URL\'s, search feature hasn\'t been implemented yet.',
    usage: '[url]',
    aliases: ['youtube', 'stream', 'ytdl'],
    cooldown: 3,
    execute(client, message, args) {
      if (message.channel.type !== 'text') return;

          const { voiceChannel } = message.member;
          const ytdl = require("ytdl-core");
          let url = args

          //Some settings to detect frauds.

          if (!voiceChannel) {
              return message.reply('please join a voice channel first!');
          }
      
          if (!args[0]) {
              return message.reply("provide a link boi.")
          }
      
          const linkcheck = ["http"];
          if(!linkcheck.some(word => message.content.includes(word)) ) {
            message.reply("that...isn't even a URL.");
            return;
          }
      
          const httpscheck = ["https"];
          if(!httpscheck.some(word => message.content.includes(word)) ) {
            message.reply("it's not even HTTPS, of course it's not a YouTube link.");
            return;
          }
      
          const youtubecheck = ['https://','youtu'];
          if(!youtubecheck.some(word => message.content.includes(word)) ) {
            message.reply("maybe that's a proper link...just not a YouTube one.");
            return;
          }
      
          voiceChannel.join().then(connection => {
              const videoname = ytdl.getInfo(`${url}`, function(err, info) {
              message.reply("now playing: " + "`\n" + `${info.title}` + "\n`") // Songname
              });
              const stream = ytdl(`${url}`, { filter: 'audioonly' });
              const dispatcher = connection.playStream(stream);

              dispatcher.on('end', () => voiceChannel.leave());
              dispatcher.on('end', () => message.channel.send("Audio stopped."));
          });
    }
}