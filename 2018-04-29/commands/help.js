const { prefix } = require('../config.json');
module.exports = {
    name: 'help',
    description: 'List all of my commands or info about a specific command.',
    usage: '[OPTIONAL: command name]',
    aliases: ['commands'],
    cooldown: 3,
    execute(client, message, args) {
        const { commands } = message.client;
        const data = [];

        if (!args.length) {
            data.push('Here\'s a list of all my commands:\n```');
            data.push(commands.map(command => command.name).join('\n'));
            data.push('```\nYou can send `' + `${prefix}help [command name]` + '` to get info on a specific command!');
        }
        else {
            if (!commands.has(args[0])) {
                return message.reply('that\'s not a valid command!');
            }

            const command = commands.get(args[0]);

            data.push(`**Name:** ${command.name}`);

            if (command.description) data.push(`**Description:** ${command.description}`);
            if (command.aliases) data.push(`**Aliases:** ${command.aliases.join(', ')}`);
            data.push(`**Cooldown:** ${command.cooldown || 3} second(s)`);
            if (command.usage) data.push(`\n**Usage:** ${prefix}${command.name} ${command.usage}`);
        }
        message.channel.send(data, { split: true })
          
    },
};