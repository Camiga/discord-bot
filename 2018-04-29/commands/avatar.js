module.exports = {
    name: 'avatar',
    description: 'Get the avatar of whoever you mention, or yourself if you don\'t mention anyone.',
    usage: '[OPTIONAL: mention]',
    aliases: ['icon', 'pfp'],
    cooldown: 5,
    execute(client, message, args) {
      if (!message.mentions.users.size) {
          return message.channel.send(`\`${message.author.tag}\` avatar oof: ${message.author.displayAvatarURL}`);
      }

      const avatarList = message.mentions.users.map(user => {
          return `\`${user.tag}\`'s avatar: ${user.displayAvatarURL}`;
      });

      // send the entire array of strings as a message
      // by default, discord.js will `.join()` the array with `\n`
      message.channel.send(avatarList);
    }
}