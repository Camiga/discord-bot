const { prefix } = require('../config.json');
module.exports = {
    name: 'translate',
    description: "Translate using Google Translate! Use" + prefix + "translate [translate-to] [text]`\n\nThe [translate-from] language is auto-detected. The [translate-to] language requires a code. \nFind it at: https://cloud.google.com/translate/docs/languages",
    usage: '[text]',
    cooldown: 5,
    execute(client, message, args) {
      let [exitlang] = args;
      let text = args.slice(1).join(" ");
      const translate = require('google-translate-api');
      
      //processing message
      message.channel.send('Processing...please wait.')
        .then(msg => {
          
      
      //fraud checking.
      if (!args[0]) {
        message.channel.send("Provide a language to translate too. Learn more with `" + prefix + "help translate` to learn more.")
        //delete processing message
        msg.delete()
        return;
      }

      translate(`${text}`, {to: `${exitlang}`}).then(res => {
      console.log(res);
      console.log(res.text);
      //=> Ik spea Nederlands!
      console.log(res.from.text.autoCorrected);
      //=> false
      console.log(res.from.text.value);
      //=> I [speak] Dutch!
      console.log(res.from.text.didYouMean);
      //=> true
      //delete the processing message at the top
      msg.delete()
      message.channel.send("__**Translate output:**__\n\n'" + res.text + "'");
      
    }).catch(err => {
      console.error(err);
      message.channel.send(":x: Error! You may not have used the correct [translate-to] code. Do `" + prefix + "help translate` to learn more.")
        //brackets for the message.reply processing up at the top.....missaligned maybe...remove these if removing processing message.
        })
    });
    }
}