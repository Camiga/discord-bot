module.exports = {
    name: 'aesthetics',
    description: 'Write anything in ＡＥＳＴＨＥＴＩＣＳ',
    usage: '[text]',
    aliases: ['aesthetic', 'aes'],
    cooldown: 2,
    execute(client, message, args) {
      const aesthetics = require("aesthetics")
      let text = args.slice(0).join(" ")
      if (!args[0]) {
        message.reply("gimme something to aesthetics mate.")
        return;
      }
      message.delete(200);
      message.channel.send(aesthetics(text))
    }
}