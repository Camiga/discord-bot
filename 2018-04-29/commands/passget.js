const { prefix } = require('../config.json');
module.exports = {
    name: 'getnsfw',
    description: 'Get a pass to the NSFW channel.',
    usage: '\n', //none
    aliases: ['nsfwget'],
    cooldown: 5,
    execute(client, message, args) {
      
      //define some stuff
      let nsfw = message.guild.roles.find("name", "NSFW Pass");
      let member = message.member;

      //check to make sure that person doesn't already have the role
      if (message.member.roles.find("name", "NSFW Pass")) {
        message.channel.send("You already have the role!")
        return;
      }

      //gives them a prompt.
      message.channel.send("***WARNING! The NSFW channel is not for everyone. Only accept if you really want to use it. DON'T ACCEPT JUST OUT OF CURIOSITY!***\nOnce you have that sorted out, type 'CONFIRM' without the quotation marks in capitals. Otherwise, this promt will cancel in 45 seconds.")
      .then(() => {
          message.channel.awaitMessages(response => response.content === 'CONFIRM', {
      max: 1,
      time: 45000,
      errors: ['time'],
    })
    .then((collected) => {
        member.addRole(nsfw).catch(console.error);
        message.channel.send("You have been given the role. Have fun?\n***Note: You can always remove the role with '" + prefix + "removensfw***'");
      })
      .catch(() => {
        message.channel.send("Prompt timeout (45 seconds have passed). Thank you for keeping your eyes safe!");
      });
      })
    }
}