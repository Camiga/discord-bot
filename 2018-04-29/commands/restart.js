const { ownerID } = require('../config.json');
module.exports = {
    name: 'restart',
    description: 'Restart the bot, OWNER ONLY.',
    usage: '\n',
    cooldown: 10,
    execute(client, message, args) {
      
      //make sure it's the owner
      if(message.author.id !== ownerID) {
        message.reply("get out")
        return
      }
      
      process.exit();
    }
}