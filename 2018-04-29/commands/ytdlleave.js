module.exports = {
    name: 'leave',
    description: 'Leave the voice channel the bot is currently in.',
    usage: '\n',
    aliases: ['disconnect', 'stop'],
    cooldown: 3,
    execute(client, message, args) {
      const { voiceChannel } = message.member;
      voiceChannel.leave()
      message.channel.send("...")
    }
}