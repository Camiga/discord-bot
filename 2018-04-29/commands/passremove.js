module.exports = {
    name: 'removensfw',
    description: 'Remove your pass to the NSFW channel like a good boi (/gal)',
    usage: '\n', //none
    aliases: ['nsfwremove'],
    cooldown: 5,
    execute(client, message, args) {
      //define more stuff
      let nsfw = message.guild.roles.find("name", "NSFW Pass");
      let member = message.member;

      //makes sure they actually have the role to remove it.
      if(!message.member.roles.find("name", "NSFW Pass")) {
        message.channel.send("You can't remove a role you don't have, dummy.")
        return;
      }

      //actually gives them the role once verified that they don't have it.
      else
        member.removeRole(nsfw).catch(console.error);
        message.channel.send("Your role has been removed. Thanks for your company?")
    }
}