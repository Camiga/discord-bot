module.exports = {
    name: 'say',
    description: 'Make the bot say something!',
    usage: '[text]',
    aliases: ['echo'],
    cooldown: 1,
    execute(client, message, args) {
      let [word1plain] = args;
      let text = args.slice(1).join(" ");
      
      if (!args[0]) {
        message.reply("oof I can't say nothing.");
        return;
      }
      
      message.delete(200);
      message.channel.send(`${word1plain} ` + text);
    }
}