const { ownerID } = require('../config.json');
module.exports = {
    name: 'eval',
    description: 'Make the bot evaluate some code. (OWNER ONLY!)',
    usage: '<code to evaluate>',
    aliases: ['evaluate'],
    cooldown: 3,
    execute(client, message, args) {
      if(message.author.id !== ownerID) return;
      const clean = text => {
        if (typeof(text) === "string")
          return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
      else
            return text;
      };
      try {
        const code = args.join(" ");
        let evaled = eval(code);

        if (typeof evaled !== "string")
          evaled = require("util").inspect(evaled);

        message.channel.send(clean(evaled), {code:"xl"});
      } catch (err) {
        message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
      }
    }
  }