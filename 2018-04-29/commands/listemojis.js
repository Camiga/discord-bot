module.exports = {
    name: 'listemojis',
    description: 'List all of the emojis in the current server. Useful for demonstating your works of art.',
    usage: '\n',
    aliases: ['allemojis','emojis'],
    cooldown: 1,
    execute(client, message) {
      const emojiList = message.guild.emojis.map(e=>e.toString()).join(" ");
      message.channel.send(emojiList);
    }
}