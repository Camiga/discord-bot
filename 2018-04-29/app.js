// Get the express module to ping the bot every 5 minutes. Look at the idiots guide. This is also being supported by Uptime Robot.
const http = require('http');
const express = require('express');
const app = express();
app.get("/", (request, response) => {
  console.log(Date.now() + " Ping Received");
  response.sendStatus(200);
});
app.listen(process.env.PORT);
setInterval(() => {
  http.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 280000);

//THIS IS WHERE YOU CAN ADD CODE FROM VISUAL STUDIO. THE ABOVE LINES PING THE BOT EVERY 5 MINUTES
//VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
//Pick up the libraries/commands from the top here
const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./config.json");
const fs = require("fs");

const { prefix, token } = require('./config.json');

// Catches any unhandled promise rejections. Avoids that damn depracation warning!
process.on('unhandledRejection', error => console.error(`Uncaught Promise Rejection:\n${error}`));

client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands');
for (const file of commandFiles) {
    const command = require(`./commands/${file}`);

    // set a new item in the Collection
    // with the key as the command name and the value as the exported module
    client.commands.set(command.name, command);
}

const cooldowns = new Discord.Collection();

//Print text in the CMD when the bot is finished starting up
client.on("ready", () => {
  console.log("The bot finished loading. Check your discord!");
  console.log(`Started at ${new Date()}`);
});

//Set game to be playing.
client.on('ready', () => {
  client.user.setActivity("with Motu and Patlu")
});

client.on("message", (message) => {
  if (!message.content.startsWith(prefix) || message.author.bot) return; //fixed! anti-botception and prefix check.
  
  //Arguments
  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const commandName = args.shift().toLowerCase();
  
  
  const command = client.commands.get(commandName)
      || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

  if (!command) return;
  
  if (command.guildOnly && message.channel.type !== 'text') {
    return message.reply('I can\'t execute that command inside DMs!');
  }
  
  if (command.args && !args.length) {
      let reply = `You didn't provide any arguments, ${message.author}!`;

      if (command.usage) {
          reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
      }

      return message.channel.send(reply);
  }
  
  if (!cooldowns.has(command.name)) {
    cooldowns.set(command.name, new Discord.Collection());
  }

  const now = Date.now();
  const timestamps = cooldowns.get(command.name);
  const cooldownAmount = (command.cooldown || 0) * 1000;

  if (!timestamps.has(message.author.id)) {
      if (!timestamps.has(message.author.id)) {
      timestamps.set(message.author.id, now);
      setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
    }
  }
  else {
      const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

      if (now < expirationTime) {
          const timeLeft = (expirationTime - now) / 1000;
          return message.reply(`oof wait ${timeLeft.toFixed(1)} second(s) before you can use \`${command.name}\` command.`);
      }

      timestamps.set(message.author.id, now);
      setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
  }

  try {
      command.execute(client, message, args); // Made it worse... apparently my one is different --> command.execute(client, message, args); ah yes it does change
  }
  catch (error) {
      console.error(error);
      message.reply('there was an error trying to execute that command!');
  }
  
});

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//CODE LINE ENDS HERE.
//THIS TOKEN IS STORED IN THE .env FILE. IT IS NOT COMPATIBLE WITH THE VISUAL STUDIO CODE VERSION!
client.login(process.env.TOKEN);

//Resume from https://anidiotsguide_old.gitbooks.io/discord-js-bot-guide/content/getting-started/the-long-version.html