# Discord Bot

Archives of an old Discord Bot I used to host until 2020-02-06 (YYYY-MM-DD). This was my first "real" program.

These archives are almost exact copies of their .tgz files downloaded from https://glitch.com/.

Discord ID's and tokens (while mostly invalid now) have been replaced with "[REMOVED]" in any applicable text or scripts.

Files created by Glitch (e.g. .glitch-assets, public/, views/, server.js, .git/), files created by its Ubuntu container (e.g. .viminfo, .data/, .cache/, .config/, .bash_history) and files containing user data (e.g. scores.sqlite, scores.sqlite-shm, scores.sqlite-wal) have all been deleted.
