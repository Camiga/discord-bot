module.exports = {
	name: 'dab',
	description: 'DSEDAB',
	usage: false,
	aliases: false,
	cooldown: 2,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		message.channel.send({
			embed: {
				color: 0xFF8000,
				title: 'ＡＡＪ　ＨＡ　ＴＯＰＩＣ　ＨＡＩ　Ｄ　ＳＥ　ＤＡＢ！',
				// url for title
				url: 'http://www.nickindia.com/',
				author: {
					name: 'Chotu',
					// url  for author icon
					icon_url: 'https://cdn.glitch.com/[REMOVED]%2Fchotuface.png?[REMOVED]',
					// url for when you click author name
					url: 'http://www.nickindia.com/',
				},
				description: 'Motu Patlu? Present! Gattu Battu? Present! Ninja? Present! Kenichi? PRESENT!',
				thumbnail: {
					url: 'https://cdn.glitch.com/[REMOVED]%2Fnicklogo.jpg?[REMOVED]',
				},
				image: {
					url: 'https://cdn.glitch.com/[REMOVED]%2Fdab.gif?[REMOVED]',
				},
				timestamp: new Date(),
				footer: {
					text: 'Nick India™',
					icon_url: 'https://cdn.glitch.com/[REMOVED]%2Fchotupoint.png?[REMOVED]',
				},
			},
		});
	},
};
