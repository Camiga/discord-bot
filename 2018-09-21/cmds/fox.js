module.exports = {
	name: 'fox',
	description: 'Get a random fox to appear in your conversation.',
	usage: false,
	aliases: ['randomfox'],
	cooldown: 3,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		const snekfetch = require('snekfetch');

		message.reply('retrieving your fox, please wait...')
			.then((dogNotify) => {

				async function getFox() {
					const { body } = await snekfetch.get('https://randomfox.ca/floof/');

					await dogNotify.edit('Done!', {
						embed: {
							title: 'Your fox(es):',
							color: 0xFF8000,
							image: {
								url: body.image,
							},
						},
					});
				}

				getFox();
				// ending brackets for edit message up top
			});
	},
};