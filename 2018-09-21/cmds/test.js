const { prefix } = require('../config.json');
module.exports = {
	name: 'test',
	description: 'Yet to be written...',
	usage: false,
	aliases: false,
	cooldown: 0,
	args: false,
	guildOnly: true,
	ownerOnly: true,
	deleteOnUse: true,
	async execute(client, message, args, Discord) {
		const { commands } = message.client;
		const api = [];
		const guilds = [];
		const utility = [];
		const other = [];
		if (!args.length) {
			const helpEmbed = new Discord.RichEmbed()
				.setTitle('Nick India Bot Help Utility')
				.setAuthor('Nick India Bot', client.user.displayAvatarURL)
				.setColor(0xFF8000)
				.setDescription(`Thanks for opening up the help utility! If you need help for a specific command, you can type \`${prefix}help <command-name>\`.`)
				.setFooter('Need more help? Ask the creator of the bot!', client.user.displayAvatarURL)
				.setTimestamp()
				.addField('API', 'placeholder', true)
				.addField('Only in Guilds', 'placeholder', true)
			// .addBlankField(true)
				.addField('Utility', 'placeholder', true)
				.addField('Other?', 'placeholder', true);

			message.channel.send(helpEmbed);
		}
		else {
			message.channel.send('oof');
		}
	},
};