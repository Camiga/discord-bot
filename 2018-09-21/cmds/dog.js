module.exports = {
	name: 'dog',
	description: 'Get a random dog to appear in your conversation.',
	usage: false,
	aliases: ['randomdog'],
	cooldown: 3,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		const fetch = require('node-fetch');

		message.reply('retrieving your dog, please wait...')
			.then((dogNotify) => {

				const number = Math.round((Math.random() * 100 + 0));
				console.log(number);
				if(number >= 50) getDog1();
				if(number < 50) getDog2();

				async function getDog1() {
					fetch('https://dog.ceo/api/breeds/image/random')
						.then(res => res.json())
						.then(json => {

							dogNotify.edit('Done!', {
								embed: {
									title: 'Your dog(s):',
									color: 0xFF8000,
									image: {
										url: json.message,
									},
								},
							});
							// end brackets for fetch
						});
				}

				async function getDog2() {
					fetch('https://random.dog/woof.json')
						.then(res => res.json())
						.then(json => {

							// sometimes the output from this URL is not an image
							if(json.url.endsWith('mp4')) {
								console.log('not an image, trying again');
								return getDog2();
							}

							dogNotify.edit('Done!', {
								embed: {
									title: 'Your dog(s):',
									color: 0xFF8000,
									image: {
										url: json.url,
									},
								},
							});
							// ending brackets for fetch
						});
				}
				// ending brackets for edit message up top
			});
	},
};