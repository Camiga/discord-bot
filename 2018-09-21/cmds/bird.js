module.exports = {
	name: 'bird',
	category: 'API',
	description: 'Get a random birb to appear in your conversation.',
	usage: false,
	aliases: ['birb', 'randombird', 'randombirb'],
	cooldown: 3,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message) {
		const fetch = require('node-fetch');

		message.reply('retrieving your birb, please wait...')
			.then((birdNotify) => {

				async function getBird() {
					fetch('http://random.birb.pw/tweet.json')
						.then(res => res.json())
						.then(json => {

							birdNotify.edit('Done!', {
								embed: {
									title: 'Your birb(s):',
									color: 0xFF8000,
									image: {
										url: `http://random.birb.pw/img/${json.file}`,
									},
								},
							});
							// end brackets for fetch
						});
				}

				getBird();

				// ending brackets for edit message up top
			});
	},
};