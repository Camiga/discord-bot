module.exports = {
	name: 'eyechart',
	description: 'Make a custom eyechart! Just input your text and wait.',
	usage: '[text]',
	aliases: false,
	cooldown: 5,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args) {
		const text = args.join('');

		message.channel.send('Processing...please wait.')
			.then(eyeChart => {

				// original crop value is:
				// &crop=377,380,0,0

				// default
				if (!args[0]) {
					eyeChart.edit('Since you didn\'t provide any text, here\'s a template:', {
						embed: {
							color: 0xFF8000,
							image: {
								url: 'https://images.weserv.nl/?url=www.eyechartmaker.com/demo.png&crop=300,380,40,0',
							},
						},
					});
					return;
				}

				eyeChart.edit('Done!', {
					embed: {
						// url for title
						title: 'Your eyechart:',
						url: 'http://www.eyechartmaker.com/demo.png',
						color: 0xFF8000,
						image: {
							// url for image
							url: `https://images.weserv.nl/?url=www.eyechartmaker.com/generate.php?line1=${text}&crop=300,380,40,0`,
						},
					},
				});
				// ending brackets for processing message
			});
	},
};