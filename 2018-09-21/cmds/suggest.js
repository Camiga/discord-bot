module.exports = {
	name: 'poll',
	description: 'Create a poll! Only works in the main guild of this bot.',
	usage: 'oof',
	aliases: ['suggest', 'createpoll'],
	cooldown: 600,
	args: true,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: true,
	async execute(client, message, args, Discord) {
		if(message.guild.id == [REMOVED]) {
			const channel = message.guild.channels.find(ch => ch.name === 'polls');
			const Webhook = await channel.createWebhook(message.author.username, message.author.avatarURL);

			const Embed = await new Discord.RichEmbed()
				.setTitle('I got a poll for you:')
				.setColor(0xFF8000)
				.setDescription(args.join(' '))
				.setFooter('Nick India Bot Suggestions', client.user.avatarURL)
				.setTimestamp();

			const suggestion = await Webhook.send(Embed).catch(console.error);
			await suggestion.react('✅');
			await suggestion.react('❌');
			await Webhook.delete();
			await message.reply('alright, we made your poll a thing. Check it out! (We pinged you.)');
			const Ping = await channel.send(`<@${message.author.id}>`);
			await Ping.delete(300);
		}
		else {return message.reply('that feature doesn\'t work here, sorry. It\'s exclusive to my home guild.');}
	},
};
