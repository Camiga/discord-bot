module.exports = {
	name: 'points',
	description: 'View your current level of points.',
	usage: false,
	aliases: ['score'],
	cooldown: 5,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		const score = client.getScore.get(message.author.id, message.guild.id);
		return message.reply(`You currently have ${score.points} points and are level ${score.level}!`);
	},
};