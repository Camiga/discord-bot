module.exports = {
	name: 'restart',
	description: 'Restart the bot, OWNER ONLY.',
	usage: false,
	aliases: ['refresh', 're'],
	cooldown: 7,
	args: false,
	guildOnly: false,
	ownerOnly: true,
	deleteOnUse: false,
	async execute(client, message) {
		// defining moment-timezone
		const moment = require('moment-timezone');
		moment.tz.setDefault('Australia/Sydney');

		await message.reply('exiting the process; will automatically restart in a few seconds...');
		await console.log(`Manual shutdown started on ${moment().format('DD-MM-YYYY h:mm:ssA zz')}`);
		await process.exit();
	},
};