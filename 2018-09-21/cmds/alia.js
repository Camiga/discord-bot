module.exports = {
	name: 'alia',
	description: 'Compose an Ali-A intro using nothing but emotes. [Mostly just a test of animated emotes. Bots get free nitro for emotes.]',
	usage: '-space',
	aliases: false,
	cooldown: 5,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args) {
		if(args[0] === '-space') return message.channel.send('<a:Ali_1:[REMOVED]> <a:Ali_2:[REMOVED]>\n<a:Ali_3:[REMOVED]> <a:Ali_4:[REMOVED]>');
		else message.channel.send('<a:Ali_1:[REMOVED]><a:Ali_2:[REMOVED]>\n<a:Ali_3:[REMOVED]><a:Ali_4:[REMOVED]>');
	},
};
