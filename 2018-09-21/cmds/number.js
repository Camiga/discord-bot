module.exports = {
	name: 'randomnumber',
	description: 'Generate a random number using pure JavaScript!',
	usage: '[minimum-number] [maximum-number]',
	aliases: ['number'],
	cooldown: 1,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args) {
		const [number1, number2] = args;

		if (!args[1]) {
			message.reply('you didn\'t give me at least 2 numbers');
			return;
		}

		if (isNaN(Math.round((Math.random() * number2) + number1))) return message.reply('your answer is Not a Number.');

		message.channel.send(Math.round((Math.random() * number2) + number1));
	},
};