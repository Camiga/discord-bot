module.exports = {
	name: 'getnsfw',
	description: 'Official way to get the NSFW Pass on Patlu International Airport.',
	usage: false,
	aliases: false,
	cooldown: 15,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message) {

		// define some stuff
		const nsfw = message.guild.roles.find('name', 'NSFW Pass');
		const member = message.member;
		const { prefix } = require('../config.json');
		const Discord = require('discord.js');

		// check to make sure that person doesn't already have the role
		if (message.member.roles.find('name', 'NSFW Pass')) {
			message.channel.send('You already have the role!');
			return;
		}

		const nsfwMessage = new Discord.RichEmbed()
			.setColor('0xFF8000')
			.setTitle('NSFW Pass confirmation')
		// .setURL('https://discord.js.org/')
		// .setAuthor('Some name', 'https://i.imgur.com/wSTFkRM.png', 'https://discord.js.org')
			.setDescription('***WARNING! The NSFW channel is not for everyone. Only accept if you really want to use it. DON\'T ACCEPT JUST OUT OF CURIOSITY!***')
		// .setThumbnail('https://i.imgur.com/wSTFkRM.png')
		// .addField('Regular field title', 'Some value here')
		// .addBlankField()
			.addField('To confirm:', 'React with ✅. The bot has already added it for you.', true)
			.addField('To decline:', 'Leave this message alone. The prompt will time out in about 15 seconds.', true)
		// .addField('Inline field title', 'Some value here', true)
		// .setImage('https://i.imgur.com/wSTFkRM.png')
			.setTimestamp();
		// .setFooter('Some footer text here', 'https://i.imgur.com/wSTFkRM.png');

		message.channel.send(nsfwMessage)
			.then((nsfwPlaceholder) => {
				const filter = (reaction, user) => {
					return reaction.emoji.name === '✅' && user.id === message.author.id;
				};

				const collector = nsfwPlaceholder.createReactionCollector(filter, { time: 15000 });

				collector.on('collect', () => {
					member.addRole(nsfw).catch(console.error);
					message.reply('you have been given the role. Have fun?\n***Note: You can always remove the role with \'' + prefix + 'removensfw***\'');
					return;
				});

				collector.on('end', () => {
					nsfwPlaceholder.delete(300);
				});

				nsfwPlaceholder.react('✅');
			});
	},
};