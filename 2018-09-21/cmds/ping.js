module.exports = {
	name: 'ping',
	description: 'Find the MS of the bot.',
	usage: '\n',
	aliases: ['ms'],
	cooldown: false,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		const time = new Date().getTime() - message.createdTimestamp;
		message.channel.send(':envelope_with_arrow: = ' + time + 'ms\n\n' + ':heartbeat: = ' + Math.round(client.ping) + 'ms\n' + '\n' + '***PONG!*** :ping_pong:');
	},
};