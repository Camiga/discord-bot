module.exports = {
	name: 'take',
	description: 'Remove points from a user. OWNER ONLY!',
	usage: '[@mention#0000] [number]',
	aliases: ['remove'],
	cooldown: 5,
	args: true,
	guildOnly: true,
	ownerOnly: true,
	deleteOnUse: false,
	execute(client, message, args) {
		// define score really quick
		const score = client.getScore.get(message.author.id, message.guild.id);

		const user = message.mentions.users.first() || client.users.get(args[0]);
		if(!user) return message.reply('You must mention someone or give their ID!');

		const pointsToAdd = parseInt(args[1], 10);
		if(!pointsToAdd) return message.reply('You didn\'t tell me how many points to remove...');

		// Get their current points.
		let userscore = client.getScore.get(user.id, message.guild.id);
		// It's possible to give points to a user we haven't seen, so we need to initiate defaults here too!
		if (!userscore) {
			userscore = { id: `${message.guild.id}-${user.id}`, user: user.id, guild: message.guild.id, points: 0, level: 1 };
		}
		userscore.points -= pointsToAdd;

		// We also want to update their level (but we won't notify them if it changes)
		const userLevel = Math.floor(0.2 * Math.sqrt(score.points));
		userscore.level -= userLevel;

		// And we save it!
		client.setScore.run(userscore);

		return message.channel.send(`${user.tag} has had ${pointsToAdd} points removed and now stands at ${userscore.points} points.`);
	},
};