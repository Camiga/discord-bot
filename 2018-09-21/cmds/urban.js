module.exports = {
	name: 'urban',
	description: 'Search Urban Dictionary.',
	usage: '[text]',
	aliases: false,
	cooldown: 3,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args, Discord) {
		message.reply('searching...please wait.')
			.then((startSearch) => {

				// async function doSearch() {
				const declaredAsAsync = async () => {

					// try for error catching
					try {
						const snekfetch = require('snekfetch');
						const { body } = await snekfetch.get('https://api.urbandictionary.com/v0/define').query({ term: args.join(' ') });
						const trim = (str, max) => (str.length > max) ? `${str.slice(0, max - 3)}...` : str;

						if (body.list == '') return startSearch.edit(`No results found for: **${args.join(' ')}**`);

						const [answer] = body.list;

						if (answer.example == '') {
							// send urban dictionary embed just without the example
							const embed = new Discord.RichEmbed()
								.setColor(0xFF8000)
								.setTitle(answer.word)
								.setURL(answer.permalink)
								.setDescription(trim(answer.definition, 12048))
								.addField('Example', 'No example was provided.')
								.addField('Rating', `${answer.thumbs_up} thumbs up.\n${answer.thumbs_down} thumbs down.`)
								.setFooter(answer.author);

							return startSearch.edit(embed);
						}
						else {
							// send urban dictionary embed normally
							const embed = new Discord.RichEmbed()
								.setColor(0xFF8000)
								.setTitle(answer.word)
								.setURL(answer.permalink)
								.setDescription(trim(answer.definition, 2048))
								.addField('Example', trim(answer.example, 1024))
								.addField('Rating', `${answer.thumbs_up} thumbs up.\n${answer.thumbs_down} thumbs down.`)
								.setFooter(answer.author);

							return startSearch.edit(embed);
						}

					}
					catch (error) {
						console.log(error);
						startSearch.edit(`Not a single result found for: **${args.join(' ')}**`);
					}
				};

				declaredAsAsync();
			});

	},
};