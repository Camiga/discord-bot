const { prefix } = require('../config.json');
module.exports = {
	name: 'help',
	description: 'List all of my commands or info about a specific command.',
	usage: '[command-name]',
	aliases: ['commands'],
	cooldown: 3,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args) {
		const { commands } = message.client;
		const data = [];

		if (!args.length) {
			data.push('Here\'s a list of all my commands:\n```');
			data.push(commands.map(command => command.name).join('\n'));
			data.push('```\nYou can send `' + `${prefix}help [command name]` + '` to get info on a specific command!');
			// send help info to dms instead of channel
			message.channel.send(data, { split: true });
			return;
		}
		else {
			if (!commands.has(args[0])) {
				return message.reply('that\'s not a valid command!');
			}

			const command = commands.get(args[0]);

			data.push(`**Name:** ${command.name}`);

			if (command.description) data.push(`**Description:** ${command.description}`);
			if (command.aliases) data.push(`**Aliases:** ${command.aliases.join(', ')}`);
			if (command.cooldown) data.push(`**Cooldown:** ${command.cooldown} seconds`);

			// if there's a usage and it's required
			if (command.usage && command.args) data.push(`\n**Usage:**\n  ${prefix}${command.name} ${command.usage}`);
			// if there's a usage and it's NOT required
			else if (command.usage && !command.args) data.push(`\n**Usage:**\n  ${prefix}${command.name}\n  ${prefix}${command.name} ${command.usage}`);
			// if there's no usage at all
			else if (!command.usage) data.push(`\n**Usage:**\n  ${prefix}${command.name}`);
		}
		message.channel.send(data, { split: true });

	},
};