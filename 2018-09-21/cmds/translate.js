const { prefix } = require('../config.json');
module.exports = {
	name: 'translate',
	description: 'Translate using Google Translate! \n\nThe [translate-from] language is auto-detected. The [translate-to] language requires a code. \nFind it at: https://cloud.google.com/translate/docs/languages',
	usage: '[translate-to] [text]',
	aliases: false,
	cooldown: 5,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args, Discord) {
		const [exitlang] = args;
		const text = args.slice(1).join(' ');
		const translate = require('google-translate-api');

		// processing message
		message.channel.send('Processing...please wait.')
			.then(msg => {


				// fraud checking.
				if (!args[1]) {
					// edit processing message
					msg.edit('Provide a language to translate to, and some text to translate. Type `' + prefix + 'help translate` to learn more.');
					return;
				}

				translate(`${text}`, { to: `${exitlang}` }).then(res => {
					// [object Object]
					console.log(res);
					// translate output
					console.log(res.text);

					// => Ik spea Nederlands!
					// true or false whether it is autocorrected or not

					if (res.from.text.autoCorrected === true) {
						// autocorrect value
						const embed = new Discord.RichEmbed()
							.setColor('0xFF8000')
							.addField('I have autocorrected what you typed to:', res.from.text.value);

						message.channel.send(embed);
					}

					// => false
					// => I [speak] Dutch!
					// true or false whether didYouMean was applied or not (didn't automatically autocorrect)
					if(res.from.text.didYouMean === true) {
						const embed = new Discord.RichEmbed()
							.setColor('0xFF8000')
							.addField('Did you mean:', res.from.text.value);

						message.channel.send(embed);
					}

					// => true
					// delete the processing message at the top
					// res.text from up the top
					msg.edit('Translation from `' + res.from.language.iso + '` to `' + exitlang + '`.\n\n' + res.text);

				}).catch(err => {
					console.error(err);
					msg.edit(':x: Error! You may not have used the correct [translate-to] code. Do `' + prefix + 'help translate` to learn more.');
					// brackets for the message.reply processing up at the top.....missaligned maybe...remove these if removing processing message.
				});
			});
	},
};