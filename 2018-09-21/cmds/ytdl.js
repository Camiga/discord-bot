module.exports = {
	name: 'play',
	description: 'Play a video URL from YouTube. Only URL\'s, search feature hasn\'t been implemented yet.',
	usage: '[url]',
	aliases: ['youtube', 'stream', 'ytdl'],
	cooldown: 3,
	args: true,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args) {
		const { voiceChannel } = message.member;
		const ytdl = require('ytdl-core');
		const url = args;

		// detect if member is in a voice channel / check if link is from youtube
		if (!voiceChannel) {
			return message.reply('please join a voice channel first!');
		}

		const linkCheck = ['https://www.youtube.com/watch?v=', 'https://youtu.be/', 'https://m.youtube.com/watch?v='];
		if(!linkCheck.some(word => message.content.includes(word))) {
			message.reply('that\'s not a playable YouTube link.');
			return;
		}


		ytdl.getInfo(`${url}`, function(err, info) {

			// function for formating time nicely
			const time = info.length_seconds;
			function fancyTimeFormat() {
				// Hours, minutes and seconds
				const hrs = ~~(time / 3600);
				const mins = ~~((time % 3600) / 60);
				const secs = time % 60;

				// Output like "1:01" or "4:03:59" or "123:03:59"
				let ret = '';

				if (hrs > 0) {
					ret += '' + hrs + ':' + (mins < 10 ? '0' : '');
				}

				ret += '' + mins + ':' + (secs < 10 ? '0' : '');
				ret += '' + secs;
				return ret;
			}

			// prepare to stream
			message.channel.send(`Now playing: **${info.title}**
:white_circle:─────────────────
:track_previous: :arrow_forward: :track_next: 0:00 / ${fancyTimeFormat(time)}
○─── :sound: ᴴᴰ :gear:`)
				.then((messageWait) => {
					voiceChannel.leave();
					voiceChannel.join().then(connection => {

						// edit the message to make it look like song is playing
						messageWait.edit(`Now playing: **${info.title}**
:white_circle:─────────────────
:track_previous: :pause_button: :track_next: 0:01 / ${fancyTimeFormat(time)}
───○ :loud_sound: ᴴᴰ :gear:`);

						const stream = ytdl(`${url}`, { filter: 'audioonly' });
						const dispatcher = connection.playStream(stream);


						dispatcher.on('end', () => {
							voiceChannel.leave();

							// edit message to show the song has finished
							messageWait.edit(`Now playing: **${info.title}**
──────────────────:white_circle:
:track_previous: :arrow_forward: :track_next: ${fancyTimeFormat(time)} / ${fancyTimeFormat(time)}
○─── :sound: ᴴᴰ :gear:`);
						});
					});

					// end brackets for ytdl info
				});
			// end brackets for edit message up top
		});
	},
};