module.exports = {
	name: 'leave',
	description: 'Leave the voice channel the bot is currently in.',
	usage: false,
	aliases: ['disconnect', 'stop'],
	cooldown: 3,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		const { voiceChannel } = message.member;

		if(!voiceChannel) {
			message.reply('you aren\'t in a voice channel though.');
			return;
		}

		voiceChannel.leave();
		message.react('✅');
	},
};