module.exports = {
	name: 'cat',
	description: 'Get a random cat instantly in the middle of your conversation!',
	usage: false,
	aliases: ['randomcat', 'kitty', 'kitten'],
	cooldown: 3,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		message.reply('retrieving your cat, please wait...')
			.then((cat) => {

				async function sendCat() {
					// try (for error catching)
					try {
						const fetch = require('node-fetch');
						fetch('http://aws.random.cat/meow')
							.then(res => res.json())
							.then(json => {

								cat.edit('Done!', {
									embed: {
										title: 'Your cat(s):',
										color: 0xFF8000,
										image: {
											url: json.file,
										},
									},
								});
								// end brackets for node-fetch
							});
					}
					catch(error) {
						sendCat();
						console.log(error);
						return;
					}
				}

				sendCat();

				// closing brackets for edit message
			});
	},
};