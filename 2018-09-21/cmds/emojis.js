module.exports = {
	name: 'listemojis',
	description: 'List all of the emojis in the current server. Useful for demonstating your works of art.',
	usage: false,
	aliases: ['allemojis', 'emojis'],
	cooldown: 1,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		const emojiList = message.guild.emojis.map(e=>e.toString()).join(' ');
		message.channel.send(emojiList);
	},
};