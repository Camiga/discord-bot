module.exports = {
	name: 'leaderboard',
	description: 'See all the scores in the server!',
	usage: false,
	aliases: ['lb'],
	cooldown: 5,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		// definitions
		const SQLite = require('better-sqlite3');
		const sql = new SQLite('./scores.sqlite');
		const Discord = require('discord.js');

		const top10 = sql.prepare('SELECT * FROM scores WHERE guild = ? ORDER BY points DESC LIMIT 10;').all(message.guild.id);

		// Now shake it and show it! (as a nice embed, too!)
		const embed = new Discord.RichEmbed()
			.setTitle('Leaderboard')
			.setAuthor(client.user.username, client.user.avatarURL)
			.setDescription('Our top 10 points leaders!')
			.setColor(0xFF8000);

		for(const data of top10) {
			embed.addField(client.users.get(data.user).tag, `${data.points} points (level ${data.level})`);
		}
		return message.channel.send({ embed });
	},
};