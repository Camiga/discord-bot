module.exports = {
	name: 'fakejoin',
	description: 'Fakes a user joining the server (client side; doesn\'t affect other bots). OWNER ONLY!',
	usage: false,
	aliases: ['fj'],
	cooldown: 0,
	args: false,
	guildOnly: true,
	ownerOnly: true,
	async execute(client, message) {
		client.emit('guildMemberAdd', message.member || await message.guild.fetchMember(message.author));
		message.channel.send('done');
	},
};