// Get the express module to ping the bot every 5 minutes. This is also being supported by Uptime Robot. This is Glitch.come exclusive to keep the bot awake and does not need to be used elsewhere.
const http = require('http');
const express = require('express');
const app = express();

// moment-timezone for better times
const moment = require('moment-timezone');
moment.tz.setDefault('Australia/Sydney');

// console.log any requests (with moment)
app.get('/', (request, response) => {
	console.log(`[${moment().format('DD-MM-YYYY h:mm:ss zz')}] - Pinged`);
	response.sendStatus(200);
});

app.listen(process.env.PORT);
setInterval(() => {
	http.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`);
	// changed from 280000 (about 4.67 minutes) to 150000 (about 2.5 minutes) for faster pinging
}, 150000);

// code starts here
// Pick up the libraries/commands from the top here
const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require('fs');
const { prefix, ownerID } = require('./config.json');
const Canvas = require('canvas');
const snekfetch = require('snekfetch');

// sqlite requirements
const SQLite = require('better-sqlite3');
const sql = new SQLite('./scores.sqlite');

// Catches any unhandled promise rejections. Avoids that damn depracation warning!
process.on('unhandledRejection', error => console.error(`Uncaught Promise Rejection:\n${error}`));

client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./cmds');
for (const file of commandFiles) {
	const command = require(`./cmds/${file}`);

	// set a new item in the Collection
	// with the key as the command name and the value as the exported module
	client.commands.set(command.name, command);
}

const cooldowns = new Discord.Collection();

// on ready event for setting sqlite table. read anidiots.guide for more info
client.on('ready', () => {
	// Check if the table "points" exists.
	const table = sql.prepare('SELECT count(*) FROM sqlite_master WHERE type=\'table\' AND name = \'scores\';').get();
	if (!table['count(*)']) {
		// If the table isn't there, create it and setup the database correctly.
		sql.prepare('CREATE TABLE scores (id TEXT PRIMARY KEY, user TEXT, guild TEXT, points INTEGER, level INTEGER);').run();
		// Ensure that the "id" row is always unique and indexed.
		sql.prepare('CREATE UNIQUE INDEX idx_scores_id ON scores (id);').run();
		sql.pragma('synchronous = 1');
		sql.pragma('journal_mode = wal');
	}

	// And then we have two prepared statements to get and set the score data.
	client.getScore = sql.prepare('SELECT * FROM scores WHERE user = ? AND guild = ?');
	client.setScore = sql.prepare('INSERT OR REPLACE INTO scores (id, user, guild, points, level) VALUES (@id, @user, @guild, @points, @level);');

	// set playing status and print in console
	client.user.setActivity('with leftover samosas | n!help');
	console.log(`Startup complete as of: ${moment().format('dddd, MMMM YYYY - h:mm:ss a zz')}`);
});

// message in case of disconnect
client.on('disconnect', () =>{
	console.log(`Lost connection at: [${moment().format('dddd, MMMM YYYY - h:mm:ss a zz')}]`);
});

// message in case of reconnecting
client.on('reconnecting', () => {
	console.log(`Attempting to reconnect at: [${moment().format('dddd, MMMM YYYY - h:mm:ss a zz')}]`);
	process.exit();
});

// when a member is added to the guild
client.on('guildMemberAdd', async member => {
	if(member.guild.id == [discord user.id removed]) {
	// Pass the entire Canvas object because you'll need to access its width, as well its context
		const applyText = (canvas, text) => {
			const ctx = canvas.getContext('2d');

			// Declare a base size of the font
			let fontSize = 70;

			do {
			// Assign the font to the context and decrement it so it can be measured again
				ctx.font = `${fontSize -= 10}px URW Gothic`;
			// Compare pixel width of the text to the canvas minus the approximate avatar size
			} while (ctx.measureText(text).width > canvas.width - 300);

			// Return the result to use in the actual canvas
			return ctx.font;
		};

		const channel = member.guild.channels.find(ch => ch.name === 'taxi-dropoffs');
		if (!channel) return;

		const canvas = Canvas.createCanvas(640, 360);
		const ctx = canvas.getContext('2d');

		const background = await Canvas.loadImage('./dab.png');
		ctx.drawImage(background, 0, 0, canvas.width, canvas.height);

		ctx.strokeStyle = '#74037b';
		ctx.strokeRect(0, 0, canvas.width, canvas.height);

		// Slightly smaller text placed above the member's display name
		ctx.font = '30px URW Gothic';
		ctx.fillStyle = '#ffffff';
		ctx.fillText('Oofs, please welcome:', canvas.width / 2.4, canvas.height / 4);

		// Add an exclamation point here and below
		ctx.font = applyText(canvas, `${member.user.username}`);
		ctx.fillStyle = '#fffb00';
		// ctx.fillText(`${member.displayName}`, canvas.width / 2.3, canvas.height / 2.5);
		ctx.fillText(`${member.user.username}`, canvas.width / 2.3, canvas.height / 2.5);

		// small text under username
		ctx.font = '18px URW Gothic';
		ctx.fillStyle = '#ffffff';
		ctx.fillText(`We now have ${member.guild.members.size} members!`, canvas.width / 2.0, canvas.height / 1.3);

		ctx.beginPath();
		ctx.arc(425, 210, 50, 0, Math.PI * 2, true);
		ctx.closePath();
		ctx.clip();
		// ctx.stroke()

		const { body: buffer } = await snekfetch.get(member.user.displayAvatarURL);
		const avatar = await Canvas.loadImage(buffer);
		ctx.drawImage(avatar, 375, 160, 100, 100);

		const attachment = new Discord.Attachment(canvas.toBuffer(), 'welcome-image.png');

		channel.send(`Welcome to the server, ${member}!`, attachment);
		// end
	}
	else {return;}
});

// when a member is removed from the guild
client.on('guildMemberRemove', async member => {
	const channel = member.guild.channels.find(ch => ch.name === 'taxi-dropoffs');
	if (!channel) return;

	channel.send(`***${member.user.username}*** has left the server. Have fun, and thanks for flying with us!`);
});

// on message (async added, but may not be 100% needed)
client.on('message', async message => {

	// anti botception (can be disabled with //)
	if (message.author.bot) return;

	// points sytem guild requirement
	let score;
	if (message.guild) {
		// This is where we'll put our code.
		score = client.getScore.get(message.author.id, message.guild.id);
		if (!score) {
			score = { id: `${message.guild.id}-${message.author.id}`, user: message.author.id, guild: message.guild.id, points: 0, level: 1 };
		}

		// going to add a cooldown to getting points
		score.points++;
		const curLevel = Math.floor(0.2 * Math.sqrt(score.points));
		if(score.level < curLevel) {
			message.reply(`You've leveled up to level **${curLevel}**! Ain't that dandy?`);
			score.level++;
		}
		client.setScore.run(score);
	}

	// prefix checking (MUST BE PUT AFTER POINT SYSTEM SO THAT IT REACTS TO MESSAGES WITHOUT PREFIXES!)
	if (!message.content.startsWith(prefix)) return;

	// Arguments
	const args = message.content.slice(prefix.length).trim().split(/ +/g);
	const commandName = args.shift().toLowerCase();


	const command = client.commands.get(commandName)
      || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

	// if (!command) return;
	if (!command) return message.reply('that\'s not a command! Do `' + prefix + 'help` to see our commands.');

	// simplify guildOnly commands
	if (command.guildOnly && message.channel.type !== 'text') return message.reply('I can\'t execute that command inside DMs!');

	// simplify ownerOnly commands
	if (command.ownerOnly && message.author.id !== ownerID) return message.reply('that command is off limits to anyone but the owner!');

	// delete messages once used (in guilds)
	if (command.deleteOnUse && message.channel.type == 'text') await message.delete(100);

	if (command.args && !args.length) {
		let reply = `You didn't provide any arguments, ${message.author}!`;

		if (command.usage) {
			reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
		}

		return message.channel.send(reply);
	}

	if (!cooldowns.has(command.name)) {
		cooldowns.set(command.name, new Discord.Collection());
	}

	const now = Date.now();
	const timestamps = cooldowns.get(command.name);
	const cooldownAmount = (command.cooldown || 0) * 1000;

	if (!timestamps.has(message.author.id)) {
		if (!timestamps.has(message.author.id)) {
			timestamps.set(message.author.id, now);
			setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
		}
	}
	else {
		const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

		if (now < expirationTime) {
			const timeLeft = (expirationTime - now) / 1000;
			return message.reply(`Oof wait ${timeLeft.toFixed(1)} second(s) before you can use the \`${command.name}\` command.`);
		}

		timestamps.set(message.author.id, now);
		setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
	}

	try {
		command.execute(client, message, args, Discord);
	}
	catch (error) {
		console.error(error);
		message.reply(`Yep, something went wrong over here.\n\`\`\`${error}\`\`\``);
	}
});

// Code ends here.
// Token is stored in the .env
client.login(process.env.TOKEN);
