// Make errors look better (remove this to debug).
process.on('unhandledRejection', e => console.error(`Unhandled Rejection:\n${e}`));

// Requirements
const Discord = require('discord.js');
const client = new Discord.Client();

const fs = require('fs');
const Canvas = require('canvas');
const fetch = require('node-fetch');
const crypto = require('crypto');

const SQLite = require('better-sqlite3');
const sql = new SQLite('./scores.sqlite');

const moment = require('moment-timezone');
moment.tz.setDefault('Australia/Sydney');

// Create website using native http module.
const http = require('http');

http.createServer(function(req, res) {
	// Pipe "Project not found page" since there is nothing else to display.
	fs.readFile('index.html', function(err, data) {
		res.write(data);
		res.end();
		console.log(`[${moment().format('DD/MM/YYYY hh:mm:ss zz')}] - Pinged`);
	});
}).listen(process.env.PORT);

// Ping every ~2.5 minutes.
const https = require('https');
setInterval(() => {
	https.get(`https://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 150000);

client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./cmds');
for (const file of commandFiles) {
	const command = require(`./cmds/${file}`);

	// set a new item in the Collection
	// with the key as the command name and the value as the exported module
	client.commands.set(command.name, command);
}

const cooldowns = new Discord.Collection();

// On startup:
client.on('ready', () => {
	// Check if the table "points" exists.
	const table = sql.prepare('SELECT count(*) FROM sqlite_master WHERE type=\'table\' AND name = \'scores\';').get();
	if (!table['count(*)']) {
		// If the table isn't there, create it and setup the database correctly.
		sql.prepare('CREATE TABLE scores (id TEXT PRIMARY KEY, user TEXT, guild TEXT, points INTEGER, level INTEGER);').run();
		// Ensure that the "id" row is always unique and indexed.
		sql.prepare('CREATE UNIQUE INDEX idx_scores_id ON scores (id);').run();
		sql.pragma('synchronous = 1');
		sql.pragma('journal_mode = wal');
	}

	// And then we have two prepared statements to get and set the score data.
	client.getScore = sql.prepare('SELECT * FROM scores WHERE user = ? AND guild = ?');
	client.setScore = sql.prepare('INSERT OR REPLACE INTO scores (id, user, guild, points, level) VALUES (@id, @user, @guild, @points, @level);');

	// Set status to playing:
	// client.user.setActivity('with lotsa numbers | n!help');
	// Set status to watching:
	client.user.setActivity('#TeamTrees | n!help', { type: 'WATCHING' });
	console.log(`[${moment().format('DD/MM/YYYY hh:mm:ss zz')}] - Connected`);
});

// In case client disconnects:
client.on('disconnect', () =>{
	console.log(`[${moment().format('DD/MM/YYYY hh:mm:ss zz')}] - Disconnected`);
});

// In case of reconnecting:
client.on('reconnecting', () => {
	console.log(`[${moment().format('DD/MM/YYYY hh:mm:ss zz')}] - Reconnecting`);
	process.exit();
});

// wWhen a member joins:
client.on('guildMemberAdd', async member => {
	// Check for the channel first
	const channel = member.guild.channels.find(ch => ch.name === 'taxi-dropoffs');
	if (!channel) return;
	// Pass the entire Canvas object because you'll need to access its width, as well its context
	const applyText = (canvas, text) => {
		const ctx = canvas.getContext('2d');

		// Declare a base size of the font
		let fontSize = 70;

		do {
			// Assign the font to the context and decrement it so it can be measured again
			ctx.font = `${fontSize -= 10}px URW Gothic`;
			// Compare pixel width of the text to the canvas minus the approximate avatar size
		} while (ctx.measureText(text).width > canvas.width - 300);

		// Return the result to use in the actual canvas
		return ctx.font;
	};

	const canvas = Canvas.createCanvas(640, 360);
	const ctx = canvas.getContext('2d');

	const background = await Canvas.loadImage(process.env.DAB_BG);
	ctx.drawImage(background, 0, 0, canvas.width, canvas.height);

	ctx.strokeStyle = '#74037b';
	ctx.strokeRect(0, 0, canvas.width, canvas.height);

	// Slightly smaller text placed above the member's display name
	ctx.font = '30px URW Gothic';
	ctx.fillStyle = '#ffffff';
	ctx.fillText('Oofs, please welcome:', canvas.width / 2.4, canvas.height / 4);

	// Add an exclamation point here and below
	ctx.font = applyText(canvas, `${member.user.username}`);
	ctx.fillStyle = '#fffb00';
	ctx.fillText(`${member.user.username}`, canvas.width / 2.3, canvas.height / 2.5);

	// small text under username
	ctx.font = '18px URW Gothic';
	ctx.fillStyle = '#ffffff';
	ctx.fillText(`We now have ${member.guild.members.size} members!`, canvas.width / 2.0, canvas.height / 1.3);

	ctx.beginPath();
	ctx.arc(425, 210, 50, 0, Math.PI * 2, true);
	ctx.closePath();
	ctx.clip();
	// This can be used for seeing where the circle is supposed to be.
	// ctx.stroke()

	const prepareAvImg = await fetch(member.user.displayAvatarURL);
	const finalAvImg = await prepareAvImg.buffer();
	const avatar = await Canvas.loadImage(finalAvImg);
	ctx.drawImage(avatar, 375, 160, 100, 100);

	const attachment = new Discord.Attachment(canvas.toBuffer(), 'nick-welcome-image.png');

	channel.send(`Welcome to the server, ${member}!`, attachment);
});

// When a member leaves:
client.on('guildMemberRemove', async member => {
	const channel = member.guild.channels.find(ch => ch.name === 'taxi-dropoffs');
	if (!channel) return;

	channel.send(`**${member.user.username}** has left the server. Have fun, and thanks for flying with us!`);
});

// On message:
client.on('message', async message => {

	// Don't respond to bots or direct messages.
	if (message.author.bot) return;
	if (!message.guild) return;

	// Remove invites
	if (message.content.toLowerCase().includes('discord.gg/')) return message.delete(400);

	// Quick 'server' filter.
	const spamFilter = message.content.toLowerCase().replace(/[.,/#!$%^&*;:{}=\-_`~()\s]/g, '');
	//if (spamFilter.includes('server') && message.author.id == process.env.JON_ID) return message.delete(5000);

	// Point system must only work in guilds.
	let score;
	if (message.guild) {
		// This is where we'll put our code.
		score = client.getScore.get(message.author.id, message.guild.id);
		if (!score) {
			score = { id: `${message.guild.id}-${message.author.id}`, user: message.author.id, guild: message.guild.id, points: 0, level: 1 };
		}

		score.points++;
		const curLevel = Math.floor(0.2 * Math.sqrt(score.points));
		if(score.level < curLevel) {
			message.reply(`You leveled up to level **${curLevel}**! Yay fake internet points!`);
			score.level++;
		}
		client.setScore.run(score);
	}

	// Change between multiple prefixes.
	const p_a = process.env.PREFIX_A;
	const p_A = p_a.toUpperCase();
	const p_b = process.env.PREFIX_B;
	const p_B = p_b.toUpperCase();
	const p_c = `<@${client.user.id}>`;
	let prefix = p_a;

	if (message.content.startsWith(p_A)) prefix = p_A;
	else if (message.content.startsWith(p_b)) prefix = p_b;
	else if (message.content.startsWith(p_B)) prefix = p_B;
	else if (message.content.startsWith(p_c)) prefix = p_c;

	// Arguments (needs to be put before anonymous message sending).
	const args = message.content.slice(prefix.length).trim().split(/ +/g);
	const commandName = args.shift().toLowerCase();

	// Set up anonymous message sending (must be put before prefix checking again).
	if(message.content.startsWith('<<<') || message.content.endsWith('<<<')) {
		if (args < 1) return;
		await message.delete(300);
		// Create a SHA512 hash from 128 random bytes of data, and use the first 7 digits.
		const Hash = crypto.createHash('SHA512').update(crypto.randomBytes(128)).digest('hex').slice(0, 7);

		// Create webhook
		const anonWeb = await message.channel.createWebhook(Hash, process.env.D_LOGO);

		// Send content
		if(message.content.startsWith('<<<')) anonWeb.send(args.join(' '));
		else anonWeb.send(message.content.substring(0, message.content.length - 3));

		await anonWeb.delete();
	}

	// prefix checking (must be put after point system and anonymous message sending)
	if (!message.content.startsWith(prefix)) return;

	const command = client.commands.get(commandName)
      || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

	// if (!command) return;
	if (!command) return message.reply(`that's not a command! Do \`${process.env.PREFIX_A}help\` to see our commands.`);
	// simplify guildOnly commands
	if (command.guildOnly && message.channel.type !== 'text') return message.reply('I can\'t execute that command inside DMs!');
	// simplify ownerOnly commands
	if (command.ownerOnly && message.author.id !== process.env.OWNERID) return message.reply('you\'re not allowed to use that command. Sorry.');
	// if arguments are needed
	if (command.args && !args.length) {
		let reply = `You didn't provide any arguments, ${message.author}!`;
		if (command.usage) {
			reply += ` The proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
		}
		return message.channel.send(reply);
	}
	// delete messages (in guilds)
	if (command.deleteOnUse && message.channel.type == 'text') await message.delete(400);

	if (!cooldowns.has(command.name)) {
		cooldowns.set(command.name, new Discord.Collection());
	}

	const now = Date.now();
	const timestamps = cooldowns.get(command.name);
	const cooldownAmount = (command.cooldown || 0) * 1000;

	if (!timestamps.has(message.author.id)) {
		if (!timestamps.has(message.author.id)) {
			timestamps.set(message.author.id, now);
			setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
		}
	}
	else {
		const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

		if (now < expirationTime) {
			const timeLeft = (expirationTime - now) / 1000;
			return message.reply(`Oof wait ${timeLeft.toFixed(1)} second(s) before you can use the \`${command.name}\` command.`);
		}

		timestamps.set(message.author.id, now);
		setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
	}

	try {
		command.execute(client, message, args, Discord);
	}
	catch (e) {
		console.error(e);
		message.reply('something very wrong happened over here.');
	}
});

// Token is stored in the .env
client.login(process.env.TOKEN);