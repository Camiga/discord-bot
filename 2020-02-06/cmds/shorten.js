module.exports = {
	name: 'shorten',
	description: 'todo',
	usage: '[oof]',
	options: false,
	aliases: false,
	cooldown: 0,
	args: true,
	guildOnly: true,
	ownerOnly: true,
	deleteOnUse: false,
	execute(client, message, args, Discord) {
		const input = args.slice(1).join('');
		const maxChars = args[0];

		// Since this script removes characters backwards, flip the string around.
		const reverse = input
			.split('')
			.reverse()
			.join('')
			.toUpperCase();

		// Set the current output to be the new reversed string.
		let output = reverse;

		// Script will run as long as this variable is true. This will be changed to false to stop the script.
		let doWork = true;

		// These checkpoints exist so that else statements don't fire when they shouldn't.
		let checkPoint = true;
		let checkPoint2 = false;
		let checkPoint3 = false;
		let checkPoint4 = false;

		// These variables detail what the script is currently allowed to do.
		let removeSpaces = true;
		let removePunct = false;
		let removeVowels = false;
		let extremeSlice = false;

		// List of notifications.
		const process = [];

		// While loop is used to remove parts of the string one by one.
		while (doWork) {
			process.push(output.split('').reverse().join(''));
			// Remove spaces first, if the string is too long, and there are any spaces to remove.
			if (removeSpaces && output.length > maxChars && Boolean(output.match(/\s/g))) {
				// Replace whitespace with nothing.
				output = output.replace(/\s/, '');
				// If the length is now suitable and we are up to here, end the script here.
			}
			else if (checkPoint && output.length <= maxChars) {
				// Stop the loop.
				doWork = false;
				process.push('> Done at removing spaces.');
				// If the script is still up to here, but there are no more spaces to remove.
			}
			else if (checkPoint && !output.match(/\s/g)) {
				// Disable this part.
				removeSpaces = false;
				// Enable the next part.
				removePunct = true;
				// Cross this checkpoint, so that the else statements don't run out of turn..
				checkPoint = false;
				// Prime the next checkpoint, so that their else statements can run.
				checkPoint2 = true;
				process.push('> Spaces removed.');
			}

			// If we are now allowed to remove punctuation, the string is too long, and there is punctuation to remove.
			if (removePunct && output.length > maxChars && Boolean(output.match(/[^\w\s]|_/g))) {
				// Remove anything that isn't whitespace, or any words.
				output = output.replace(/[^\w\s]|_/, '');
				// End the script if the length is correct.
			}
			else if (checkPoint2 && output.length <= maxChars) {
				doWork = false;
				process.push('> Done at removing punctuation.');
			}
			else if (checkPoint2 && !output.match(/[^\w\s]|_/g)) {
				// Disable paramaters for this piece of code, and enable the next ones.
				removePunct = false;
				removeVowels = true;
				checkPoint2 = false;
				checkPoint3 = true;
				process.push('> Punctuation removed.');
			}

			// Remove vowels if needed.
			if (removeVowels && output.length > maxChars && Boolean(output.match(/[AEIOU]/g))) {
				output = output.replace(/[AEIOU]/, '');
			}
			else if (checkPoint3 && output.length <= maxChars) {
				doWork = false;
				process.push('> Done at removing vowels.');
			}
			else if (checkPoint3 && !output.match(/[AEIOU]/)) {
				removeVowels = false;
				extremeSlice = true;
				checkPoint3 = false;
				checkPoint4 = true;
				process.push('> Vowels removed.');
			}

			// If the string is still too long, crop the end of it.
			if (extremeSlice && output.length > maxChars) {
				// Reverse the string again, to bring it back to its original form.
				let reversed = output
					.split('')
					.reverse()
					.join('');
				// Remove 1 character from the string.
				reversed = reversed.substr(0, reversed.length - 1);
				// Reverse the string again, and save it as output.
				output = reversed
					.split('')
					.reverse()
					.join('');
				// Since this piece of code is guranteed to get the string down to its max length, wait for it to happen.
			}
			else if (checkPoint4 && output.length <= maxChars) {
				extremeSlice = false;
				checkPoint4 = false;
				process.push('> Done at trimming.');
				// Stop the script.
				doWork = false;
			}
		}

		// Your final output will be given after the while loop has finished.
		message.channel.send(output.split('').reverse().join(''));

		const finalEmbed = new Discord.RichEmbed()
			.setColor('FF8000')
			.setTitle('Text Shortener')
			.setDescription(process)
			.addField('Your shortened text is: ', output.split('').reverse().join(''))
			.setFooter('n!shorten | Compress your text.');

		message.channel.send(finalEmbed);
	},
};