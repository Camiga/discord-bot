module.exports = {
	name: 'rewind',
	description: 'Keep up to date on the great internet numbers floating around Youtube Rewind.',
	usage: false,
	options: false,
	aliases: ['rew'],
	cooldown: 5,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const fetch = require('node-fetch');

		// Store data once it is recieved.
		const Data = [];

		// Simple function to recieve information on a video.
		async function getVideo(Video) {
			const body = await fetch(`https://www.youtube.com/watch?v=${Video}`).then(res => res.text());

			function Get(Look, Ahead) {
				return body.substr(body.indexOf(Look), Ahead).match(/[0-9,]/g).join('');
			}

			// Find all the data and send to the array.
			await Data.push(Get('h-v', 40), Get('like this ', 50), Get('slike this ', 50));
		}

		// Rewind 2018
		await getVideo('YbJOTdZBX1g');
		// Rewind 2019
		await getVideo('2lAe1cqCOXo');

		const Result = new Discord.RichEmbed()
			.setColor('FF8000')
			.setTitle('Youtube Rewind Statistics')
			.setURL('https://youtu.be/2lAe1cqCOXo')
			.setDescription('Now available! See the latest statistics of YouTube Rewind 2018 and 2019!')
			.setThumbnail('https://cdn.glitch.com/[REMOVED]%2Fyoutube_social_squircle_red.png')
			.addBlankField()
			.addField('Rewind 2018:', `- Views:  ${Data[0]}\n- Likes:       ${Data[1]}\n- Dislikes: ${Data[2]}`, true)
			.addField('Rewind 2019:', `- Views:  ${Data[3]}\n- Likes:       ${Data[4]}\n- Dislikes: ${Data[5]}`, true)
			.setTimestamp()
			.setFooter('v0.1.0 "POC" | n!rewind | n!rew', 'https://cdn.glitch.com/[REMOVED]%2Fyoutube_social_squircle_red.png');

		message.channel.send(Result);
	},
};
