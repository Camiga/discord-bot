module.exports = {
	name: 'tts',
	description: 'Use a text-to-speech from Google Translate to speak in a voice channel! Input a language (or a code from https://cloud.google.com/translate/docs/languages if unsure), a speed (between 0 - 1, decimals allowed) and your text at the end.',
	usage: '[OPTIONS] [text]',
	options: ['[language] = Speak in a language of your choosing. Get a code from the site above, or input the language in plain text (not 100% reliable).', '[speed] = How fast the bot will talk. Is a decimal from 0 - 1. Both of these options can be in any order.'],
	aliases: ['speak'],
	cooldown: 5,
	args: true,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args) {
		const messageWait = await message.channel.send('Processing...please wait.');

		const url = require('url');
		const fetch = require('node-fetch');
		const host = 'https://translate.google.com';
		const { voiceChannel } = message.member;

		let textSlice = 2;
		let lang = args[0];
		let speed = Number(args[1]);

		// Check if in a voice channel.
		if (!voiceChannel) return messageWait.edit('Please join a voice channel.');

		// Figure out if language is real or not.
		const languages = {
			'auto': 'Automatic', 'af': 'Afrikaans', 'sq': 'Albanian',
			'ar': 'Arabic', 'hy': 'Armenian', 'az': 'Azerbaijani',
			'eu': 'Basque', 'be': 'Belarusian', 'bn': 'Bengali',
			'bs': 'Bosnian', 'ca': 'Catalan', 'zh-cn': 'Chinese Simplified',
			'zh-tw': 'Chinese Traditional', 'hr': 'Croatian', 'cs': 'Czech',
			'da': 'Danish', 'nl': 'Dutch', 'en': 'English',
			'eo': 'Esperanto', 'et': 'Estonian', 'tl': 'Filipino',
			'fi': 'Finnish', 'fr': 'French', 'de': 'German',
			'el': 'Greek', 'hi': 'Hindi', 'hu': 'Hungarian',
			'is': 'Icelandic', 'id': 'Indonesian', 'it': 'Italian',
			'ja': 'Japanese', 'jw': 'Javanese', 'km': 'Khmer',
			'ko': 'Korean', 'la': 'Latin', 'lv': 'Latvian',
			'mk': 'Macedonian', 'ml': 'Malayalam', 'mr': 'Marathi',
			'my': 'Myanmar (Burmese)', 'ne': 'Nepali', 'no': 'Norwegian',
			'pl': 'Polish', 'pt': 'Portuguese', 'ro': 'Romanian',
			'ru': 'Russian', 'sr': 'Serbian', 'si': 'Sinhala',
			'sk': 'Slovak', 'es': 'Spanish', 'su': 'Sundanese',
			'sw': 'Swahili', 'sv': 'Swedish', 'ta': 'Tamil',
			'te': 'Telugu', 'th': 'Thai', 'tr': 'Turkish',
			'uk': 'Ukrainian', 'vi': 'Vietnamese', 'cy': 'Welsh',
		};

		function getISOCode(language) {
			if (!language) return false;
			language = language.toLowerCase();
			if (language in languages) return language;

			const keys = Object.keys(languages).filter((key) => {
				if (typeof languages[key] !== 'string') return false;
				return languages[key].toLowerCase() === language;
			});

			return keys[0] || null;
		}

		function isSupported(language) {
			return Boolean(getISOCode(language));
		}

		// If there is a number where the language should be, swap order of language and speed.
		if (!isNaN(Number(args[0]))) {
			speed = Number(args[0]);
			lang = args[1];
		}

		// Check if language is correct
		const Info = [];
		if (!isSupported(lang)) {
			Info.push('Voice set to: `en` (Automatic)');
			lang = 'en';
			textSlice = textSlice - 1;
		}
		else {Info.push(`Voice set to: \`${lang}\` (Provided by user)`);}

		// Check if speed is correct
		if (isNaN(speed)) {
			speed = 1;
			textSlice = textSlice - 1;
			Info.push('Speed set to: `1` (Automatic)');
		}
		else {Info.push(`Speed set to: \`${speed}\` (Provided by user)`);}

		// Define text and check it here (must be put down here so that textSlice works correctly).
		const text = args.slice(textSlice).join(' ');
		if (text.length > 200) return messageWait.edit('Text length must be less than 200 characters. Sorry.');

		// start
		await fetch(host)
			.then(res =>res.text())
			.then(body => {

				const expressions = [
					'TKK=\'(\\d+.\\d+)\';',
					'tkk:\'(\\d+.\\d+)\'',
				];

				const matches = expressions
					.map(expr => body.match(expr))
					.filter(res => res);

				// key (with some more error handling)
				const key = matches[0][1];

				function XL(a, b) {
					for (let c = 0; c < b.length - 2; c += 3) {
						let d = b.charAt(c + 2);
						d = d >= 'a' ? d.charCodeAt(0) - 87 : Number(d);
						d = b.charAt(c + 1) == '+' ? a >>> d : a << d;
						a = b.charAt(c) == '+' ? a + d & 4294967295 : a ^ d;
					}
					return a;
				}

				let a = text, b = key;
				const d = b.split('.');
				b = Number(d[0]) || 0;
				// removed 'var' from inside for and replaced it with empty let statements.
				let e, f, g;
				for (e = [], f = 0, g = 0; g < a.length; g++) {
					let m = a.charCodeAt(g);
					m < 128 ? e[f++] = m : (m < 2048 ? e[f++] = m >> 6 | 192 : ((m & 64512) == 55296 && g + 1 < a.length && (a.charCodeAt(g + 1) & 64512) == 56320 ? (m = 65536 + ((m & 1023) << 10) + (a.charCodeAt(++g) & 1023),
					e[f++] = m >> 18 | 240,
					e[f++] = m >> 12 & 63 | 128) : e[f++] = m >> 12 | 224,
					e[f++] = m >> 6 & 63 | 128),
					e[f++] = m & 63 | 128);
				}
				a = b;
				for (f = 0; f < e.length; f++) {
					a += e[f];
					a = XL(a, '+-a^+6');
				}
				a = XL(a, '+-3^+b+-f');
				a ^= Number(d[1]) || 0;
				a < 0 && (a = (a & 2147483647) + 2147483648);
				a = a % 1E6;

				// token
				const token = a.toString() + '.' + (a ^ b);

				// end result
				const result = host + '/translate_tts' + url.format({
					query: {
						ie: 'UTF-8',
						q: text,
						tl: getISOCode(lang) || 'en',
						total: 1,
						idx: 0,
						textlen: text.length,
						tk: token,
						client: 't',
						prev: 'input',
						ttsspeed: speed || 1,
					},
				});

				// Leave first just to ensure nothing else is playing.
				voiceChannel.leave();
				voiceChannel.join().then(connection => {
					const dispatcher = connection.playStream(result);

					messageWait.edit(`**Playing TTS with:**\n${Info.join('\n')}`);

					dispatcher.on('end', () => {
						voiceChannel.leave();
					});
				});
				// end of fetch
			}).catch(e => {
				messageWait.edit('An error has occured. Please try again later.');
				console.log(e);
			});
	},
};