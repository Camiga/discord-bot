module.exports = {
	name: 'purge',
	description: 'A command to delete messages in bulk.',
	usage: '[OPTIONS] [ammount]',
	options: '[@mention] = Get `[ammount]` of messages and only delete them from the user mentioned.',
	aliases: ['delete', 'prune', 'p'],
	cooldown: 0,
	args: true,
	guildOnly: true,
	ownerOnly: true,
	deleteOnUse: false,
	async execute(client, message) {
		const user = message.mentions.users.first();
		let amount = parseInt(message.content.split(' ')[1]) ? parseInt(message.content.split(' ')[1]) : parseInt(message.content.split(' ')[2]);

		// Add 1 to amount to delete users message too.
		if (!user) await amount++;
		else await message.delete(500);

		if (!amount) return message.reply('Must specify an amount to delete!');
		if (!amount && !user) return message.reply('Must specify a user and amount, or just an amount, of messages to purge!');

		function doDelete() {
			message.channel.fetchMessages({
				limit: amount,
			}).then((messages) => {
				if (user) {
					const filterBy = user ? user.id : client.user.id;
					messages = messages.filter(m => m.author.id === filterBy).array().slice(0, amount);
				}
				message.channel.bulkDelete(messages).catch(error => console.log(error.stack));
			});
		}

		// Delay message delete by creating a function.
		// This is to stop the "ghost messages"
		setTimeout(doDelete, 400);
	},
};