module.exports = {
	name: 'help',
	description: 'List all of my commands or info about a specific command.',
	usage: '[OPTIONS]',
	options: '[command-name] = Get help on a specific command.',
	aliases: ['commands'],
	cooldown: 3,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args) {
		const { PREFIX_A, PREFIX_B } = process.env;
		const { commands } = message.client;
		const command = commands.get(args[0]);
		const data = [];


		if (!args.length) {
			data.push('\n\nThanks for requesting help! Here are our current commands:\n```');
			data.push(commands.map(cmd => cmd.name).join(', '));
			data.push(`\`\`\`\nIf you need help for a specific command, just do \`${PREFIX_A}help [command_name]\`.\nYou can use either \`${PREFIX_A}\`, \`${PREFIX_B}\`, or a mention as the prefix.\n\n__**This bot's commands are not case sensitive.**__`);
		}

		else {
			// Verification:
			if (!commands.has(args[0])) return message.reply('that\'s not a valid command.');
			if (command.ownerOnly && !message.member.roles.find(role => role.name == 'Flight Attendants')) return message.reply('you\'re not allowed to use that command. Sorry.');

			data.push(`\n\n__**Name:**__ ${command.name}`);

			if (command.description) data.push(`__**Description:**__ ${command.description}`);
			// if there's a usage
			if (command.usage) data.push(`\n__**Usage:**__\`\`\`${PREFIX_A}${command.name} ${command.usage}\`\`\``);
			// if there's no usage at all
			else if (!command.usage) data.push(`\n__**Usage:**__\`\`\`${PREFIX_A}${command.name}\`\`\``);
			if (command.options) data.push(`__**Options:**__\`\`\`${command.options}\`\`\``);
			if (command.aliases) data.push(`__**Aliases:**__ ${command.aliases.join(', ')}`);
			if (command.cooldown) data.push(`__**Cooldown:**__ ${command.cooldown} seconds`);
		}

		// Send data
		message.reply(data, { split: true });
	},
};