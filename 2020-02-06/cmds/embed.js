module.exports = {
	name: 'embed',
	description: 'Make the bot say something, just with a nice looking embed.',
	usage: '[text]',
	options: false,
	aliases: ['sayembed'],
	cooldown: 1,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: true,
	execute(client, message, args, Discord) {
		const embed = new Discord.RichEmbed()
			.setColor(0xFF8000)
			.setDescription(args.join(' '));
		message.channel.send(embed);
	},
};