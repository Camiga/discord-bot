module.exports = {
	name: 'getnsfw',
	description: 'Get a pass to a secret channel.',
	usage: false,
	options: false,
	aliases: ['passget', 'nsfwget', 'pget'],
	cooldown: 30,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const nsfw = message.guild.roles.find(role => role.name == 'Secret Channel Pass');
		const member = message.member;
		const prefix = process.env.PREFIX_A;

		// check to make sure that person doesn't already have the role
		if (message.member.roles.find(role => role.name == 'Secret Channel Pass')) {
			message.reply('you already have the role!');
			return;
		}

		const nsfwMessage = new Discord.RichEmbed()
			.setColor('FF8000')
			.setTitle('NSFW Pass confirmation')
			.setDescription('**Note: The secret channel has questionable stuff in it. Only accept if you\'re okay with that stuff.**')
			.addField('To confirm:', 'React with a tick, the bot has already added it for you.', true)
			.addField('To decline:', 'Leave this message alone. This prompt will time out in about 60 seconds.', true);

		const notify = await message.channel.send(nsfwMessage);

		const filter = (reaction, user) => {
			return reaction.emoji.name === '✅' && user.id === message.author.id;
		};

		const collector = await notify.createReactionCollector(filter, { time: 60000 });

		collector.on('collect', () => {
			member.addRole(nsfw).catch(console.error);
			message.reply(`you have been given the role. **Note: You can always remove the role with** \`${prefix}removensfw\``);
			return;
		});

		collector.on('end', () => {
			notify.delete();
		});

		notify.react('✅');
	},
};