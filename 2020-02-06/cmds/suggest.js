module.exports = {
	name: 'poll',
	description: 'Create a poll! It will be sent to #polls and reactions will be added to it to make voting easy.',
	usage: '[poll-text]',
	aliases: ['suggest'],
	cooldown: 600,
	args: true,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: true,
	async execute(client, message, args) {
		const channel = message.guild.channels.find(ch => ch.name === 'polls');
		const Webhook = await channel.createWebhook(message.author.username, message.author.avatarURL);

		const suggestion = await Webhook.send(`**I have a poll for you:**\n\n${args.join(' ')}`);
		await suggestion.react('✅');
		await suggestion.react('❌');
		await Webhook.delete();
		await message.reply('alright, we made your poll a thing. Check it out! (We pinged you.)');
		const Ping = await channel.send(`<@${message.author.id}>`);
		await Ping.delete(300);
	},
};