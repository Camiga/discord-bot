module.exports = {
	name: 'play',
	description: 'Play a video from YouTube using a URL you must provide.',
	usage: '[url]',
	options: false,
	aliases: ['youtube', 'stream'],
	cooldown: 5,
	args: true,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args) {
		const ytdl = require('ytdl-core');
		// Drop-in replacement (kind of) to play audio that's apparently faster.
		const ytdlD = require('ytdl-core-discord');
		const { voiceChannel } = message.member;

		// Check if member is in a voice channel.
		if (!voiceChannel) {
			return message.reply('please join a voice channel first!');
		}

		// Check if link is avalid.
		const linkCheck = ['https://www.youtube.com/watch?v=', 'https://youtu.be/', 'https://m.youtube.com/watch?v='];
		if(!linkCheck.some(word => message.content.includes(word))) {
			return message.reply('that\'s not a valid link.');
		}

		// Send message, leave any channels, get video title, and join new channel.
		const Notify = await message.reply('preparing...please wait.');
		await voiceChannel.leave();
		const Info = await ytdl.getInfo(`${args}`);
		const connection = await voiceChannel.join();

		// Play stream, and edit message.
		// Note, use .playStream for ytdl, and .playOpusStream for ytdlD.
		// Begin a "stopwatch".
		const t1 = new Date();
		// Old
		// const dispatcher = await connection.playOpusStream(await ytdlD(`${args}`));
		// Supposedly faster, but uses connection.play instead. Wait for v12.
		const dispatcher = await connection.playOpusStream(await ytdlD(`${args}`, { type: 'opus', highWaterMark: 50, passes: 3, volume: false }));
		Notify.edit(`Now playing: **${Info.title}**`);

		// Leave when finished, and edit message again.
		dispatcher.on('end', () => {
			// End "stopwatch".
			const t2 = new Date();
			voiceChannel.leave();
			Notify.edit(`Finished playing: **${Info.title}** *(Played for ${(t2 - t1) / 1000} seconds.)*`);
		});
	},
};