module.exports = {
	name: 'dog',
	description: 'Get a random dog to appear in your conversation.',
	usage: false,
	options: false,
	aliases: ['randomdog'],
	cooldown: 3,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const fetch = require('node-fetch');
		const Notify = await message.reply('retrieving your dog, please wait...');

		async function getDog() {
			const Dog = await fetch('https://random.dog/woof.json').then(res => res.json());
			// sometimes the output from this URL is not an image
			if(Dog.url.endsWith('mp4')) {
				console.log('not an image, trying again');
				return getDog();
			}

			const dogEmbed = new Discord.RichEmbed()
				.setColor('FF8000')
				.setTitle('Your dog(s):')
				.setImage(Dog.url);

			await Notify.edit(dogEmbed);
		}
		getDog();
	},
};