module.exports = {
	name: 'news',
	description: 'Hmmm...',
	usage: false,
	options: false,
	aliases: false,
	cooldown: 70,
	args: true,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		if (!message.member.roles.find(role => role.name == 'Flight Attendants')) return message.reply('you\'re not allowed to use that command. Sorry.');

		function send() {
			const Hook = new Discord.WebhookClient(process.env.NEWS_ID_P, process.env.NEWS_TOKEN_P);
			Hook.send(`${message.guild.roles.find(role => role.name == 'Economy+ Seating')} ${args.slice(1).join(' ')}`);
		}

		function sendR() {
			const Hook = new Discord.WebhookClient(process.env.NEWS_ID, process.env.NEWS_TOKEN);
			Hook.send(`${message.guild.roles.find(role => role.name == 'Passengers')} ${args.join(' ')}`);
		}

		if(args[0] == '-s') {
			if (args[1] == null) return message.reply('please enter some content after `-s`.');
			message.reply('your post will be delivered in about 60 seconds. Use this time to ensure the `Economy+ Seating` role can be mentioned.');
			setTimeout(send, 60000);
		}
		else {
			// Regular
			message.reply('your post will be delivered in about 60 seconds. Use this time to ensure the `Passengers` role can be mentioned.');
			setTimeout(sendR, 60000);
		}
	},
};