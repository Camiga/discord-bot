module.exports = {
	name: 'give',
	description: 'Give points to a user.',
	usage: '[@mention] [number]',
	options: false,
	aliases: false,
	cooldown: 0,
	args: true,
	guildOnly: true,
	ownerOnly: true,
	deleteOnUse: false,
	execute(client, message, args) {
		const user = message.mentions.users.first() || client.users.get(args[0]);
		if(!user) return message.reply('You must mention someone or give their ID!');

		const pointsToAdd = parseInt(args[1], 10);
		if(!pointsToAdd) return message.reply('You didn\'t tell me how many points to give...');

		// Get their current points.
		let userscore = client.getScore.get(user.id, message.guild.id);
		// It's possible to give points to a user we haven't seen, so we need to initiate defaults here too!
		if (!userscore) {
			userscore = { id: `${message.guild.id}-${user.id}`, user: user.id, guild: message.guild.id, points: 0, level: 1 };
		}
		userscore.points += pointsToAdd;

		// And we save it!
		client.setScore.run(userscore);

		return message.channel.send(`${user.tag} has received ${pointsToAdd} points and now stands at ${userscore.points} points.`);
	},
};