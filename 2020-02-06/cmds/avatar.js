module.exports = {
	name: 'avatar',
	description: 'Get the avatar of whoever you mention, or yourself if you don\'t mention anyone.',
	usage: '[OPTIONS]',
	options: '[@mention_someone] = Get their avatar instead of your own.',
	aliases: ['icon', 'pfp'],
	cooldown: 5,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		if (!message.mentions.users.size) {
			return message.channel.send(`Your avatar: ${message.author.avatarURL}?size=1024`);
		}

		const avatarList = message.mentions.users.map(user => {
			return `\`${user.tag}\`'s avatar: ${user.avatarURL}?size=1024`;
		});

		// send the entire array of strings as a message
		// by default, discord.js will `.join()` the array with `\n`
		message.channel.send(avatarList);
	},
};