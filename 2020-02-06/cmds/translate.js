module.exports = {
	name: 'translate',
	description: 'Translate text into many different languages! Type in a language to translate to (or get it from https://cloud.google.com/translate/docs/languages if unsure) and some text to translate. The language to translate from is automatically detected.',
	usage: '[translate-to-language] [text]',
	aliases: ['trans', 'tra', 'tr'],
	cooldown: 6,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const Notify = await message.channel.send('Processing...please wait.');
		const fetch = require('node-fetch');
		const querystring = require('querystring');

		const langFrom = 'auto';
		const langTo = args[0];
		const text = args.slice(1).join(' ');

		const languages = {
			'auto': 'Automatic', 'af': 'Afrikaans', 'sq': 'Albanian',
			'am': 'Amharic', 'ar': 'Arabic', 'hy': 'Armenian',
			'az': 'Azerbaijani', 'eu': 'Basque', 'be': 'Belarusian',
			'bn': 'Bengali', 'bs': 'Bosnian', 'bg': 'Bulgarian',
			'ca': 'Catalan', 'ceb': 'Cebuano', 'ny': 'Chichewa',
			'zh-cn': 'Chinese Simplified', 'zh-tw': 'Chinese Traditional', 'co': 'Corsican',
			'hr': 'Croatian', 'cs': 'Czech', 'da': 'Danish',
			'nl': 'Dutch', 'en': 'English', 'eo': 'Esperanto',
			'et': 'Estonian', 'tl': 'Filipino', 'fi': 'Finnish',
			'fr': 'French', 'fy': 'Frisian', 'gl': 'Galician',
			'ka': 'Georgian', 'de': 'German', 'el': 'Greek',
			'gu': 'Gujarati', 'ht': 'Haitian Creole', 'ha': 'Hausa',
			'haw': 'Hawaiian', 'iw': 'Hebrew', 'hi': 'Hindi',
			'hmn': 'Hmong', 'hu': 'Hungarian', 'is': 'Icelandic',
			'ig': 'Igbo', 'id': 'Indonesian', 'ga': 'Irish',
			'it': 'Italian', 'ja': 'Japanese', 'jw': 'Javanese',
			'kn': 'Kannada', 'kk': 'Kazakh', 'km': 'Khmer',
			'ko': 'Korean', 'ku': 'Kurdish (Kurmanji)', 'ky': 'Kyrgyz',
			'lo': 'Lao', 'la': 'Latin', 'lv': 'Latvian',
			'lt': 'Lithuanian', 'lb': 'Luxembourgish', 'mk': 'Macedonian',
			'mg': 'Malagasy', 'ms': 'Malay', 'ml': 'Malayalam',
			'mt': 'Maltese', 'mi': 'Maori', 'mr': 'Marathi',
			'mn': 'Mongolian', 'my': 'Myanmar (Burmese)', 'ne': 'Nepali',
			'no': 'Norwegian', 'ps': 'Pashto', 'fa': 'Persian',
			'pl': 'Polish', 'pt': 'Portuguese', 'pa': 'Punjabi',
			'ro': 'Romanian', 'ru': 'Russian', 'sm': 'Samoan',
			'gd': 'Scots Gaelic', 'sr': 'Serbian', 'st': 'Sesotho',
			'sn': 'Shona', 'sd': 'Sindhi', 'si': 'Sinhala',
			'sk': 'Slovak', 'sl': 'Slovenian', 'so': 'Somali',
			'es': 'Spanish', 'su': 'Sundanese', 'sw': 'Swahili',
			'sv': 'Swedish', 'tg': 'Tajik', 'ta': 'Tamil',
			'te': 'Telugu', 'th': 'Thai', 'tr': 'Turkish',
			'uk': 'Ukrainian', 'ur': 'Urdu', 'uz': 'Uzbek',
			'vi': 'Vietnamese', 'cy': 'Welsh', 'xh': 'Xhosa',
			'yi': 'Yiddish', 'yo': 'Yoruba', 'zu': 'Zulu',
		};

		function getISOCode(language) {
			if (!language) return false;
			language = language.toLowerCase();
			if (language in languages) return language;

			const keys = Object.keys(languages).filter((key) => {
				if (typeof languages[key] !== 'string') return false;
				return languages[key].toLowerCase() === language;
			});

			return keys[0] || null;
		}

		function isSupported(language) {
			return Boolean(getISOCode(language));
		}

		// Error handling:
		if(!isSupported(langTo)) return Notify.edit(`The language \`${args[0]}\` is not valid.`);

		// Recieve token
		function zr(a) {
			let b;
			if (yr !== null) {b = yr;}
			else {
				b = wr(String.fromCharCode(84));
				const c = wr(String.fromCharCode(75));
				b = [ b(), b() ];
				b[1] = c();
				b = (yr = b.join(c()) || '') || '';
			}
			let d = wr(String.fromCharCode(116));
			let c = wr(String.fromCharCode(107));
			d = [ d(), d() ];
			d[1] = c();
			c = '&' + d.join('') + '=';
			d = b.split('.');
			b = Number(d[0]) || 0;

			// Replace var with let
			let e, f, g;
			for (e = [], f = 0, g = 0; g < a.length; g++) {
				let l = a.charCodeAt(g);
				l < 128 ? e[f++] = l : (l < 2048 ? e[f++] = l >> 6 | 192 : ((l & 64512) == 55296 && g + 1 < a.length && (a.charCodeAt(g + 1) & 64512) == 56320 ? (l = 65536 + ((l & 1023) << 10) + (a.charCodeAt(++g) & 1023), e[f++] = l >> 18 | 240, e[f++] = l >> 12 & 63 | 128) : e[f++] = l >> 12 | 224, e[f++] = l >> 6 & 63 | 128), e[f++] = l & 63 | 128);
			}
			a = b;
			for (f = 0; f < e.length; f++) a += e[f], a = xr(a, '+-a^+6');
			a = xr(a, '+-3^+b+-f');
			a ^= Number(d[1]) || 0;
			a < 0 && (a = (a & 2147483647) + 2147483648);
			a %= 1E6;
			return c + (a.toString() + '.' + (a ^ b));
		}

		let yr = null;
		const wr = function(a) {
			return function() {
				return a;
			};
		};

		const xr = function(a, b) {
			for (let c = 0; c < b.length - 2; c += 3) {
				let d = b.charAt(c + 2);
				d = d >= 'a' ? d.charCodeAt(0) - 87 : Number(d);
				d = b.charAt(c + 1) == '+' ? a >>> d : a << d;
				a = b.charAt(c) == '+' ? a + d & 4294967295 : a ^ d;
			}
			return a;
		};
		// End of recieve token.

		// Use text with token
		async function generate(funcText) {
			try {
				let tk = zr(funcText);
				tk = tk.replace('&tk=', '');
				return { name: 'tk', value: tk };
			}
			catch (error) {
				return error;
			}
		}

		async function translate(funcText, funcLangFrom, funcLangTo) {
			try {
				// Get ISO 639-1 codes for the languages.
				funcLangFrom = getISOCode(langFrom);
				funcLangTo = getISOCode(langTo);

				// Generate Google Translate token for the text to be translated.
				const token = await generate(funcText);

				// URL & query string required by Google Translate.
				let url = 'https://translate.google.com/translate_a/single';
				const data = {
					client: 'gtx',
					sl: funcLangFrom,
					tl: funcLangTo,
					hl: funcLangTo,
					dt: [ 'at', 'bd', 'ex', 'ld', 'md', 'qca', 'rw', 'rm', 'ss', 't' ],
					ie: 'UTF-8',
					oe: 'UTF-8',
					otf: 1,
					ssel: 0,
					tsel: 0,
					kc: 7,
					q: funcText,
					[token.name]: token.value,
				};

				// Append query string to the request URL.
				url = `${url}?${querystring.stringify(data)}`;

				let requestOptions;
				// If request URL is greater than 2048 characters, use POST method.
				if (url.length > 2048) {
					delete data.q;
					requestOptions = [
						`${url}?${querystring.stringify(data)}`,
						{
							method: 'POST',
							body: {
								q: text,
							},
						},
					];
					requestOptions[1] = JSON.stringify(requestOptions[1]);
				}
				else {
					requestOptions = [ url ];
				}

				// Request translation from Google Translate.
				const response = await fetch(requestOptions).then(res => res.json());

				const result = {
					text: '',
					from: {
						language: {
							didYouMean: false,
							iso: '',
						},
						text: {
							autoCorrected: false,
							value: '',
							didYouMean: false,
						},
					},
					raw: '',
				};

				// Raw output removed because end user would probably never need it.
				const body = (response);
				body[0].forEach((obj) => {
					if (obj[0]) {
						result.text += obj[0];
					}
				});

				if (body[2] === body[8][0][0]) {
					result.from.language.iso = body[2];
				}
				else {
					result.from.language.didYouMean = true;
					result.from.language.iso = body[8][0][0];
				}

				if (body[7] && body[7][0]) {
					let str = body[7][0];

					str = str.replace(/<b><i>/g, '[');
					str = str.replace(/<\/i><\/b>/g, ']');

					result.from.text.value = str;

					if (body[7][5] === true) {
						result.from.text.autoCorrected = true;
					}
					else {
						result.from.text.didYouMean = true;
					}
				}

				return result;
			}
			catch (e) {
				return false;
			}
		}

		// let Result = await translate(text, langFrom, langTo);
		let Result;
		async function fetchResults() {
			const resultChecker = await translate(text, langFrom, langTo);
			if (resultChecker == false) {
				console.log('Failed. Retrying...');
				await fetchResults();
			}
			else {
				Result = resultChecker;
			}
		}

		await fetchResults();

		const Errors = [];
		Notify.edit(Result.text);
		if(Result.from.text.autoCorrected) Errors.push(`Your text was autocorrected to:\n\`${Result.from.text.value}\``);
		if(Result.from.text.didYouMean) Errors.push(`Your text was not autocorrected, but you might have meant:\n\`${Result.from.text.value}\``);
		if(Result.from.text.autoCorrected || Result.from.text.autoCorrected) {
			const EndMessage = await new Discord.RichEmbed()
				.setColor('FF8000')
				.setTitle('Note:')
				.setDescription(Errors);
			message.channel.send(EndMessage);
		}
	},
};