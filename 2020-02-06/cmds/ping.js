module.exports = {
	name: 'ping',
	description: 'P O N G ! See how long it takes for the bot to respond.',
	usage: false,
	options: false,
	aliases: ['ms'],
	cooldown: false,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message) {
		const time = new Date().getTime() - message.createdTimestamp;
		message.channel.send(':envelope_with_arrow: = ' + time + 'ms\n\n' + ':heartbeat: = ' + Math.round(client.ping) + 'ms\n' + '\n' + '***PONG!*** :ping_pong:');
	},
};