module.exports = {
	name: 'eyechart',
	description: 'Make a custom eyechart! Just input your text and wait.',
	usage: '[text]',
	options: false,
	aliases: ['eye', 'chart'],
	cooldown: 5,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const Notify = await message.channel.send('Processing...please wait.');

		// Initialize Canvas.
		const Canvas = require('canvas');
		const canvas = Canvas.createCanvas(754, 800);
		const ctx = canvas.getContext('2d');

		// Download and draw image.
		const background = await Canvas.loadImage(`http://www.eyechartmaker.com/generate.php?line1=${args.join('')}`);
		ctx.drawImage(background, 0, 40, canvas.width, canvas.height);

		// Create attachment form buffer and send.
		const eyeChart = new Discord.Attachment(canvas.toBuffer(), 'eyechart.png');
		message.channel.send('Done!', eyeChart);
		Notify.delete();
	},
};