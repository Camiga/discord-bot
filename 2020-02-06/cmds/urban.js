module.exports = {
	name: 'urban',
	description: 'Search Urban Dictionary.',
	usage: '[text]',
	options: false,
	aliases: false,
	cooldown: 3,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const fetch = require('node-fetch');
		const Notify = await message.reply('searching...please wait.');
		// try for error catching
		try {
			// API URL = https://api.urbandictionary.com/v0/define?term=
			const Results = await fetch(`https://api.urbandictionary.com/v0/define?term=${args.join('')}`);
			const body = await Results.json();
			const trim = (str, max) => (str.length > max) ? `${str.slice(0, max - 3)}...` : str;

			if (body.list == '') return Notify.edit(`No results found for: **${args.join(' ')}**`);

			const [answer] = body.list;

			if (answer.example == '') {
				// send urban dictionary embed without the example
				const embed = new Discord.RichEmbed()
					.setColor('FF8000')
					.setTitle(answer.word)
					.setURL(answer.permalink)
					.setDescription(trim(answer.definition, 2048))
					.addField('Rating', `${answer.thumbs_up} thumbs up.\n${answer.thumbs_down} thumbs down.`)
					.setFooter(`Author: ${answer.author}`);

				return Notify.edit(embed);
			}
			else {
				// send urban dictionary embed normally
				const embed = new Discord.RichEmbed()
					.setColor('FF8000')
					.setTitle(answer.word)
					.setURL(answer.permalink)
					.setDescription(trim(answer.definition, 2048))
					.addField('Example', trim(answer.example, 1024))
					.addField('Rating', `${answer.thumbs_up} thumbs up.\n${answer.thumbs_down} thumbs down.`)
					.setFooter(`Author: ${answer.author}`);

				return Notify.edit(embed);
			}

		}
		catch (error) {
			console.log(error);
			Notify.edit(`An error had occurred while searching for: **${args.join(' ')}**. Sorry about that.`);
		}
	},
};