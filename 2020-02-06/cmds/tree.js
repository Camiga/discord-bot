module.exports = {
	name: 'trees',
	description: 'Display data from the #TeamTrees project.',
	usage: false,
	options: false,
	aliases: ['tree', 'tre', 'teamtrees'],
	cooldown: 10,
	args: false,
	guildOnly: true,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		const fetch = require('node-fetch');
		const htmlparser2 = require('htmlparser2');

		// Function to format large numbers properly.
		function numFormat(i) {
			// Split number into array.
			const number = i.toString().split();
			// Add ,'s wherever they belong.
			number[0] = number[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
			// Return final number after being turned back to a string.
			return number.join('');
		}

		// Function to format UTC ISO dates nicely.
		function dateFormat(i) {
			return i.replace(/T/, ' ').replace(/\..+/, '');
		}

		// Fetch website source.
		const website = await fetch('https://teamtrees.org').then(res => res.text());
		// Find the "totalTrees" value in the website source.
		const plantedStart = website.indexOf('id="totalTrees"');
		// Get ~60 characters infront of that value.
		const plantedLine = website.substr(plantedStart, 60);
		// Find the "data-count" tag. This stores the number of trees planted.
		const plantedValue = plantedLine.match(/data-count="(\d+)"/g).join('');
		// Remove any other characters, and leave just the number.
		const treesPlanted = Number(plantedValue.match(/[0-9]/g).join(''));
		const remainder = treesPlanted - 20000000;

		// Function to find leaderboard data.
		function newSearch(searchingFor) {
			// Store data temporarily in this array.
			const Data = [];
			// This array is used to combine broken segments of some user comments.
			let combinedComment = [];
			// Used to determine if the current text being searched is a user comment.
			let isComment = false;
			// Sanitize username input too.
			let combinedName = [];
			let isName = false;

			// Create a new HTML parser using `htmlparser2`.
			const parser = new htmlparser2.Parser(
				{
					onopentag(name, attribs) {
						// Check if the text being searched is a user comment.
						if (name == 'span' && attribs.class == 'd-block medium mb-0') {
							// Set flag to true if it is a user comment.
							isComment = true;
							// Reset the value if searching anything else.
						}
						else {isComment = false;}

						// Check if text is a username.
						if (name == 'strong' && attribs.class == '') {
							isName = true;
						}
						else {isName = false;}
					},
					// Main text processing done here.
					ontext(text) {
						// Don't push newlines to the array.
						if (text.match(/\r?\n|\r/g)) return;
						// If the text is part of a comment, push it to the comment array instead.
						if (isComment) return combinedComment.push(text);
						// If the text is the username, push to username array.
						if (isName) return combinedName.push(text);

						// Once comment processing is done, check if there is anything in the array.
						if (combinedComment.length > 0) {
							// Push any comments to the data array.
							Data.push(combinedComment.join(''));
							// Reset the temporary comment array.
							combinedComment = [];
						}
						// Do the same for the username array.
						if (combinedName.length > 0) {
							Data.push(combinedName.join(''));
							combinedName = [];
						}

						// If no `return` statements were used, push the current text to the data array.
						Data.push(text);
					},
				},
				// Properly encode characters like 's in user comments.
				{ decodeEntities: true },
			);

			// Find the Regex specified, and grab its starting position.
			const searchStart = website.indexOf(searchingFor);
			// Get a segment ~2600 characters long of the leaderboard.
			// Increase this number for more user comments.
			const searchText = website.substr(searchStart, 2000);

			// Replace any empty user comments in the leaderboard with '[NO COMMENT]'.
			const filteredText = searchText.replace(
				/<span class="d-block medium mb-0"><\/span>/g,
				'<span class="d-block medium mb-0">[NO COMMENT]</span>',
			);

			// Parse the edited text segment.
			parser.write(filteredText);
			// Stop the parser once finished.
			parser.end();
			// Push the final comment through.
			Data.push(combinedComment.join(''));
			// Return the finished array.
			return Data;
		}

		// Array containing the top tree donations.
		const topTrees = newSearch('id="top-donations"');
		// Array containing the newest user donations.
		const recentTrees = newSearch('id="donationFeedContent"');

		// Array for holding the data of the top trees leaderboard.
		const topTreesData = [];
		// For loop to split array into chunks of 4 lines.
		for (let i = 0; i < topTrees.length; i += 4) {
			const currentArray = topTrees.slice(i, i + 4);
			// Log the username and number of trees first.
			topTreesData.push(`__${currentArray[0]}__ **(${currentArray[1]})**`);
			// Then log their comment and time/date.
			topTreesData.push(
				`  *"${currentArray[3]}"* - \`${dateFormat(currentArray[2])} UTC\``,
			);
		}

		// Array for holding the data of the most recent trees.
		const recentTreesData = [];
		for (let i = 0; i < recentTrees.length; i += 4) {
			const currentArray = recentTrees.slice(i, i + 4);
			recentTreesData.push(`__${currentArray[0]}__ **(${currentArray[1]})**`);
			recentTreesData.push(
				`  *"${currentArray[3]}"* - \`${dateFormat(currentArray[2])} UTC\``,
			);
		}

		const treeEmbed = new Discord.RichEmbed()
			.setColor('FF8000')
			.setTitle('#teamtrees')
			.setURL('https://teamtrees.org/')
			.setDescription('We did it! But that doesn’t mean we’re done. Come back anytime you feel like planting a tree! https://teamtrees.org')
			.addField(`${numFormat(treesPlanted)} TREES PLANTED`, `The 20 million goal has been fulfilled! We are \`${numFormat(remainder)}\` trees past the goal!`)
			.addBlankField()
			.addField('Top donations:', topTreesData.join('\n'), true)
			.addField('Most recent donations:', recentTreesData.join('\n'), true)
			.setTimestamp()
			.setFooter('v1.1 | n!trees | n!tre');

		message.channel.send(treeEmbed);

	},
};