module.exports = {
	name: 'restart',
	description: 'Restart the bot.',
	usage: false,
	options: false,
	aliases: ['refresh', 're', 'r'],
	cooldown: 0,
	args: false,
	guildOnly: false,
	ownerOnly: true,
	deleteOnUse: false,
	async execute(client, message) {
		// defining moment-timezone
		const moment = require('moment-timezone');
		moment.tz.setDefault('Australia/Sydney');

		await message.channel.send('Restarting...');
		await console.log(`[${moment().format('DD/MM/YYYY hh:mm:ss zz')}] - Shutdown`);
		await process.exit();
	},
};