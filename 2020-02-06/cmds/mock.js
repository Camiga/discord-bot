module.exports = {
	name: 'rc',
	description: 'RaNdOmLy cApItAlIzE yOuR sEnTeNcEs lIkE ThIs.',
	usage: '[text]',
	options: false,
	aliases: ['randomcaps', 'randcaps', 'mock'],
	cooldown: 1,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: true,
	execute(client, message, args) {
		// This variable will alternate between 0 and 1.
		let changingBit = 0;
		// Join the words of the array together, and split them into letters.
		const inputArray = args.join(' ').split('');

		// Modify the text using .map
		const Text = inputArray.map(function(letters) {
			// Update the changing bit to either a 0 or 1 for each letter that passes through.
			changingBit ^= 1;

			// Change case of each letter depending on what the changing bit currently is.
			if (changingBit) {
				return letters.toUpperCase();
			}
			else {
				return letters.toLowerCase();
			}
		});

		// Join the array together, and send it.
		message.channel.send(Text.join(''));
	},
};