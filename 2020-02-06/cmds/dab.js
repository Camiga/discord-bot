module.exports = {
	name: 'dab',
	description: 'DSEDAB',
	usage: false,
	options: false,
	aliases: false,
	cooldown: 2,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args, Discord) {
		const dabEmbed = new Discord.RichEmbed()
			.setColor('FF8000')
			.setTitle('ＡＡＪ　ＨＡ　ＴＯＰＩＣ　ＨＡＩ　Ｄ　ＳＥ　ＤＡＢ！')
			.setURL('http://www.nickindia.com/')
			.setAuthor('Chotu', process.env.CHOTU_FACE, 'http://www.nickindia.com/')
			.setDescription('Motu Patlu? Present! Gattu Battu? Present! Ninja? Present! Kenichi? PRESENT!')
			.setThumbnail(process.env.NICK_LOGO)
			.setImage(process.env.DAB_GIF)
			.setTimestamp()
			.setFooter('Nick India', process.env.CHOTU_POINT);

		message.channel.send(dabEmbed);
	},
};