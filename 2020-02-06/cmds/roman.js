module.exports = {
	name: 'roman',
	description: 'A flexible translator for Roman and Arabic numerals.',
	usage: '[input]',
	options: '[input] - Can be either Roman or Arabic numerals. Input is translated and corrected automatically.',
	aliases: ['ro'],
	cooldown: 2,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	execute(client, message, args) {
		const Input = args[0];

		// Roman
		function Roman(i) {
			const digits = String(i).split('');
			const key = ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM',
				'', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC',
				'', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'];
			let roman = '';
			i = 3;

			// Loop for filling in Roman Numerals.
			while (i--) roman = (key[+digits.pop() + (i * 10)] || '') + roman;

			// Final answer.
			const answer = Array(+digits.join('') + 1).join('M') + roman;
			return answer;
		}

		// Arabic
		function UnRoman(i) {
			i = i.toUpperCase();
			const romanNumList = ['CM', 'M', 'CD', 'D', 'XC', 'C', 'XL', 'L', 'IX', 'X', 'IV', 'V', 'I'];
			const corresp = [900, 1000, 400, 500, 90, 100, 40, 50, 9, 10, 4, 5, 1];
			let index = 0, num = 0;
			for (const rn in romanNumList) {
				index = i.indexOf(romanNumList[rn]);
				while (index != -1) {
					num += parseInt(corresp[rn]);
					i = i.replace(romanNumList[rn], '-');
					index = i.indexOf(romanNumList[rn]);
				}
			}
			return num;
		}

		// Error handling.
		let warning = '';
		if (isNaN(Input) && Input.toUpperCase().replace(/[CDILMVX]/g, '') !== '') return message.reply('ERROR: Mixed characters detected.');
		if (Input > 3999 || UnRoman(Input) > 3999) warning = '\nWARN: Roman numerals shouldn\'t be more than 3999. Continuing anyway...\n';
		if (Input > 99999) return message.reply('ERROR: Limit of 100000 reached.');

		// Send output.
		// Roman Numerals.
		if (isNaN(Input)) return message.reply(warning + `\nArabic: ${UnRoman(Input)}\nRoman: ${Roman(UnRoman(Input))}`);
		// Arabic Numerals.
		else return message.reply(warning + `\nArabic: ${Input}\nRoman: ${Roman(Input)}`);
	},
};