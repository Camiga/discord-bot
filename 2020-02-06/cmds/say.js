module.exports = {
	name: 'say',
	description: 'Make the bot say something!',
	usage: '[text]',
	options: false,
	aliases: ['echo'],
	cooldown: 1,
	args: true,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: true,
	execute(client, message, args) {
		message.channel.send(args.join(' '));
	},
};