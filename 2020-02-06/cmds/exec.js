module.exports = {
	name: 'exec',
	description: 'Even more extra secret stuff.',
	usage: false,
	options: false,
	aliases: ['exe', 'ex'],
	cooldown: 0,
	args: true,
	guildOnly: false,
	ownerOnly: true,
	deleteOnUse: false,
	async execute(client, message, args) {
		// try for error catching
		try {
			const { exec } = require('child_process');
			const clean = text => {
				if (typeof (text) === 'string') {return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));}
				else {return text;}
			};

			const outputErr = (msg, stdData) => {
				let { stdout, stderr } = stdData;
				stderr = stderr ? ['`STDERR`', '```sh', clean(stderr.substring(0, 800)) || ' ', '```'] : [];
				stdout = stdout ? ['`STDOUT`', '```sh', clean(stdout.substring(0, stderr ? stderr.length : 2046 - 40)) || ' ', '```'] : [];
				const newMessage = stdout.concat(stderr).join('\n').substring(0, 2000);
				msg.edit(newMessage);
			};

			const doExec = (cmd, opts = {}) => {
				return new Promise((resolve, reject) => {
					exec(cmd, opts, (err, stdout, stderr) => {
						if (err) return reject({ stdout, stderr });
						resolve(stdout);
					});
				});
			};

			const command = args.join(' ');
			const outMessage = await message.channel.send(`Running \`${command}\`...`);
			let stdOut = await doExec(command).catch(data=> outputErr(outMessage, data));
			// Fix TypeError that happens here.
			if (stdOut == null) return;
			stdOut = stdOut.substring(0, 1750);
			outMessage.edit(`\`OUTPUT\`
\`\`\`sh
${clean(stdOut)}
\`\`\``);

		}
		catch(error) {
			message.channel.send(`\`ERROR\`\n\`\`\`${error}\`\`\``);
		}
	},
};