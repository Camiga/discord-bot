module.exports = {
	name: 'tseries',
	description: 'View the battle between PewDiePie and T-Series!',
	usage: false,
	aliases: ['pewdiepie', 'youtube', 't-series', 'subs', 't', 'pew'],
	cooldown: 5,
	args: false,
	guildOnly: false,
	ownerOnly: false,
	deleteOnUse: false,
	async execute(client, message, args, Discord) {
		// Start "stopwatch".
		const t1 = new Date();
		const fetch = require('node-fetch');
		const Data = [];

		// Generate numbers with a range.
		function Rand(min, max) {
			return Math.round(Math.random() * (max - min) + min);
		}

		// Send loading message.
		const Notify = await message.channel.send(`This will only take a moment.\nLoading: **${Rand(0, 20)}%**`);

		async function getSubs(Type, Name) {
		// Fetch the subscription embed.
			const body = await fetch(`https://www.youtube.com/subscribe_embed?${Type}=${Name}`).then(res => res.text());
			// "-count" contains subscribers number near it.
			// Find how far in you have to go to get there.
			const lineStart = body.indexOf('-count');
			// Get ~70 characters infront of lineStart.
			const fullLine = body.substr(lineStart, 70);
			// Use regex to get the value in the HTML tag.
			const value = fullLine.match(/>(.*?)</g).join('');
			// Regex again to get only the subscriber count.
			const Subs = value.match(/[0-9MK]/g).join('');
			// Return number of subscribers.
			await Data.push(Subs);
		}

		// Convert strings into numbers.
		function Num(i) {
			return Number(i.replace(/[,MK]/g, ''));
		}

		// Calculate difference in subscribers.
		function Diff(i1, i2) {
			// Subtract eachother.
			const Subtract = Num(i1) - Num(i2);
			// Return numbers back as strings.
			return Subtract.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
		}

		// Error catching.
		try {
			// Do functions to add numbers to array. Also show progress.
			// PewDiePie
			await getSubs('channel', 'PewDiePie');
			Notify.edit(`This will only take a moment.\nLoading: **${Rand(30, 50)}%**`);
			// T-Series
			await getSubs('channel', 'tseries');
			Notify.edit(`This will only take a moment.\nLoading: **${Rand(60, 80)}%**`);
			// Music (with a channel ID)
			await getSubs('channelid', 'UC-9-kyTW8ZkZNDHQJ6FgpwQ');
			Notify.edit(`This will only take a moment.\nLoading: **${Rand(90, 99)}%**`);


			// Break up sentences. (This isn't needed, just looks nicer)
			const Done1 = 'The ability to view the live number of subscribers has been removed.';
			const Done2 = 'You are now only given abbreviations of the current number of subscribers a channel has.';
			const Done3 = 'There is nothing I can do about that, sorry. I guess you can still look at the statistics of Rewind.';

			// Change what is displayed depending on numbers.
			const pewDiePie = ' SUBSCRIBER COUNTS ARE GONE!! '.replace(/ /g, ':crab::crab:');
			const pewdGap = `T-Series is __**${Diff(Data[1], Data[0])}M**__ subscribers ahead of PewDiePie.`;
			const behindMusic = `PewDiePie is ${Diff(Data[2], Data[0])}M subscribers **behind** Music.`;
			const aheadMusic = `T-series is ${Diff(Data[1], Data[2])}M subscribers **ahead** of Music.`;

			// End "stopwatch".
			const t2 = new Date();
			const Results = await new Discord.RichEmbed()
				.setColor('FF8000')
				.setTitle('T-Series vs Music vs PewDiePie')
				.setURL('https://www.youtube.com/user/PewDiePie?sub_confirmation=1')
				.setDescription(`${Done1} ${Done2} ${Done3}`)
				.addBlankField()
				.addField('The numbers:', `T-Series:       ${Data[1]}\nMusic:          ${Data[2]}\n__**PewDiePie:**__   __**${Data[0]}**__`, true)
				.addField('Rewind 2018:', 'Moved to `n!rewind`.', true)
				.addField('Other numbers:', `- ${behindMusic}\n- ${aheadMusic}`)
				.addField(pewdGap, pewDiePie)
				.addBlankField()
				.setTimestamp()
				.setFooter(`v4.1.0 | Update results with n!t | Done in ${t2 - t1}ms`);

			Notify.delete(1000);
			await message.reply(Results);
		}
		catch(e) {
			Notify.edit('**An error has occured, please try again later.**');
			return console.error(e);
		}
	},
};