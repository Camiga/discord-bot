Information is no longer stored in a config.js
It is stored in the .env file.
To call on whatever you need, use `process.env.[NAME]` in your code.

At the top of app.js, that command will ping the bot every ~2.5 minutes. Uptimerobot will also ping the bot every 5 minutes.

watch.json prevents the bot from restarting every time you make 1 small change. Instead, it will check the files for changes every ~15 minutes, and only restart if there are any changes.

Throttle in watch.json is 900000 by default.

Node.js version is determined in the package.json, under 
"engines": {
  "node": "10.x"
  
This can be determined by changing the "node" value, or you can set it to a major release with x after it, to automatically determine the highest LTS verson (highest node 10).

Optional packages for discord.js:
uws (deprecated, use @discordjs/uws with discordjs 12 and later)
sodium
libsodium-wrappers
bufferutil
erlpack (there is both a github link and an unofficial npm link)
zlib-sync (discordjs 12 and later)

config.json is no longer used, everything is now stored in the .env file.